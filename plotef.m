function [fh] = plotef(filename,name,includeTrue)
  if (nargin<3)
    includeTrue = false;
  end
  dat = csvread(filename);
  s = dat(:,1);
  v = dat(:,2);
  k = dat(:,4);
  ok = dat(:,5);
  if (includeTrue)
    e = dat(:,end-3);
  else
    e = dat(:,end);
  end
  %if (size(dat,2)>5)    
  %  oklab = 'other capital';
  %else 
  oklab = 'x';
  %end
  
  figure;
  scatter3(k,ok,s,5,e)
  xlabel('own capital')
  ylabel(oklab)
  %ylabel('eta')
  zlabel('invest');
  title(['policy function' name]);
  %colorbar();
  set(gca,'YDir','reverse');
  fh(1) = gcf;
  
  figure;
  scatter3(k,ok,v,5,e)
  xlabel('capital')
  ylabel(oklab);
  %ylabel('eta')
  zlabel('value');
  title(['value function' name]);
  %colorbar();
  set(gca,'YDir','reverse');
  fh(2) = gcf;
  
  if (includeTrue)
    vt = dat(:,end);
    figure;
    scatter3(k,ok,vt,5,e)
    xlabel('capital')
    ylabel(oklab);
    %ylabel('eta')
    zlabel('value');
    title(['value function true' name]);
    %colorbar();
    set(gca,'YDir','reverse');
    %goodaxis = axis();
    fh(3) = gcf;    
    
    %figure(2);
    %axis(goodaxis);
    quantile(v,0:0.1:1)
    quantile(vt,0:0.1:1)
    mean((v-vt).^2)
    
    figure;
    scatter3(k,ok,v-vt,5,e)
    xlabel('capital')
    ylabel(oklab);
    %ylabel('eta')
    zlabel('value');
    title(['value function error' name]);
    %colorbar();
    set(gca,'YDir','reverse');
    fh(4) = gcf;    
  end  
end