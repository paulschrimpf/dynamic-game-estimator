/* Use thrust library for GPGPU */
#include <thrust/device_vector.h>
#include <thrust/inner_product.h>
#include <vector>
#include <iostream>
using std::vector;
using std::cout;
using std::endl;

template <typename T> 
struct squareDiff
{
  __host__ __device__
  T operator()(const T &x, const T &y) const {
    return((x-y)*(x-y));
  } 
};

template<typename T>
T cudaEpanKernel(const vector<T> &x, 
                      const vector<T> &y, 
                      const T bw, const int order)   
{
  T k;
  thrust::device_vector<T> gx = x, gy = y;
  //thrust::device_vector gR(x.size()*x.size());
  k = 0.46875; //= 15/32
  // set u2 = norm(x-y)^2
  T u2 = thrust::inner_product(gx.begin(),gx.end(),gy.begin(), 
                                   0.0, thrust::plus<T>(),squareDiff<T>());
  u2 /= (bw*bw);
  if (u2<1) {
    if (order==4) k = 0.46875*(7*u2*u2-10*u2+3)/bw;
    else if (order==2) k = 0.75*(1-u2)/bw;
    else { 
      cout << "Kernel order " << order << " not implemented" << endl;
      exit(1);
    }
  }
  return((T) k);
}
