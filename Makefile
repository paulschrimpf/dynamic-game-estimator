CPP = g++
CC = gcc
NVCC = /opt/cuda/bin/nvcc
# Non standard libraries needed: cuda, magma, acml (can be replaced by
# any version of lapack and blas), amdlibm (can be removed and
# -mveclibabi=acml flag should be deleted), nlopt

CUDA = /opt/cuda
MAGMA = /opt/magma
ACMLLIB = /usr/lib/acml/gfortran

LIB = 	-L$(MAGMA)/lib -L$(CUDA)/lib64 -L$(ACMLLIB) \
	-lacml \
	-lamdlibm \
	-lgomp -lnlopt -lgfortran -lcuda \
	-lcudart -lcublas -lmagma -ldl \
	-lprofiler 

INC =  -I$(MAGMA)/include -I$(CUDA)/include -I/usr/include/acml/gfortran

LDLIB = -Wl,-rpath,$(ACMLLIB) \
	-Wl,-rpath,$(CUDA)/lib64 \
	-Wl,-rpath,$(MAGMA)/lib 


DEF = -DHAVE_STD -DHAVE_NAMESPACES -DHAVE_CUBLAS -D_AMD
CFLAGS = -Ofast -funroll-loops -march=native -ffast-math -m128bit-long-double -fopenmp -Wall \
	-DNDEBUG  -pipe -Wno-deprecated-declarations -fomit-frame-pointer \
	-fassociative-math -freciprocal-math -fno-signed-zeros \
	-funsafe-loop-optimizations -funsafe-math-optimizations -flto -flto-report \
	-fwhole-program \


DFLAGS = -g -march=native -Wall -D_DEBUG -fopenmp #-mno-avx -Ofast -ffast-math # -fbounds-check

PGENFLAGS = -fprofile-dir=/home/paul/dynamicGames/gas/simulation/prof \
	-fprofile-generate 
PUSEFLAGS = -Wcoverage-mismatch -fprofile-use \
	-fprofile-dir=/home/paul/dynamicGames/gas/simulation/prof \
	-fprofile-correction 

PFLAGS = $(PUSEFLAGS)

CPPFLAGS = $(CFLAGS) 

NVFLAGS = -arch sm_21 -x cu -O2
DNVFLAGS = -arch sm_21 -x cu -g

CPPSRC  = $(wildcard *.cpp)
CSRC = $(wildcard *.c)
OBJ = $(patsubst %.cpp,obj/%.o,$(CPPSRC)) \
	$(patsubst %.c,obj/%.o,$(CSRC)) \
	obj/cudaKernel.o
DOBJ = $(patsubst obj/%.o, dobj/%.o, $(OBJ))

game: obj game.exe
	echo "Finished building game.exe"

debug: dobj game-debug.exe	
	echo $(OBJ) $(DOBJ) "Finished building game-debug.exe"

obj: 
	mkdir obj

dobj: 
	mkdir dobj

prof: 
	mkdir prof

prof/game.gcda: game-pgen.exe
	./game-pgen.exe param-pgen.txt

obj/random.o: random.c random.h 
	$(CC) random.c $(DEF) $(CFLAGS) $(PFLAGS) -c -o $@

obj/cudaKernel.o: cudaKernel.cu
	$(NVCC) $(NVFLAGS) cudaKernel.cu -c -o $@

obj/linalg.o: linalg.h linalg.cpp
	$(CPP) $(INC) linalg.cpp $(DEF) $(CPPFLAGS) $(PFLAGS) -c -o $@

obj/kernel.o: kernel.hpp kernel.cpp misc.hpp
	$(CPP) $(INC) kernel.cpp $(DEF) $(CPPFLAGS) $(PFLAGS) -c -o $@

obj/misc.o: misc.hpp misc.cpp
	$(CPP) $(INC) misc.cpp $(DEF) $(CPPFLAGS) $(PFLAGS) -c -o $@


obj/gameRBF.o: gameRBF.cpp kernel.hpp linalg.h misc.hpp obj
	$(CPP) $(INC) $(CPPFLAGS) $(DEF) $(PFLAGS) -c gameRBF.cpp -o $@

################################################################################
dobj/random.o: random.c random.h 
	$(CC) random.c $(DEF) $(DFLAGS) -c -o $@

dobj/cudaKernel.o: cudaKernel.cu 
	$(NVCC) $(DNVFLAGS) cudaKernel.cu -c -o $@

dobj/kernel.o: kernel.hpp kernel.cpp misc.hpp
	$(CPP) $(INC) kernel.cpp $(DEF) $(DFLAGS) -c -o $@

dobj/misc.o: misc.hpp misc.cpp  
	$(CPP) $(INC) misc.cpp $(DEF) $(DFLAGS) -c -o $@

dobj/linalg.o: linalg.h linalg.cpp 
	$(CPP) $(INC) linalg.cpp $(DEF) $(DFLAGS) -c -o $@

dobj/gameRBF.o: gameRBF.cpp kernel.hpp linalg.h 
	$(CPP) $(INC)  $(DEF) $(DFLAGS) -c gameRBF.cpp -o $@

##################################################################################
game-pgen.exe: Makefile $(OBJ)
	$(CPP) $(CPPFLAGS) $(PGENFLAGS) $(OBJ) $(LIB) $(LDLIB) \
	-o game-pgen.exe

game.exe: Makefile $(OBJ)  prof/game.gcda
	$(CPP) $(CPPFLAGS) $(PFLAGS) $(OBJ) $(LIB) $(LDLIB) \
	-o game.exe

game-debug.exe: Makefile $(DOBJ) 
	$(CPP) $(INC) $(DFLAGS) $(DOBJ) $(LIB) $(LDLIB) \
	-o game-debug.exe

efunc0.csv: game.exe param.txt
	./game.exe param.txt

results: efunc0.csv
	echo "Finished running game.exe"

clean:
	rm -f *.exe *.o obj/*.o dobj/*.o

spotless:
	rm -f *.exe *.o prof/* obj/*.o dobj/*.o
