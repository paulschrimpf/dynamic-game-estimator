clear; close all;
set(0,'defaultlinelinewidth',3);
set(0,'defaultaxeslinewidth',2);
set(0,'defaultaxesfontsize',18);
fh = plotf('func.csv','');

if (1)
  plotef('efunc.csv',' est');
  plotef('efunc0.csv',' e0',true);
  
  idat = csvread('eta.csv');
  idat = idat(abs(idat(:,3))<10,:);
  inv = idat(:,1); eta = idat(:,2); ehat = idat(:,3);
  figure;
  scatter3(eta,ehat,inv);
  xlabel('eta')
  ylabel('etahat');
  zlabel('invest');
  set(gca,'YDir','reverse');
  
  %figure;
  %foo = csvread('valueMSE.csv');
  %[bar ind] = sort(foo(:,1));
  %foo = foo(ind,:);
  %plot(foo(:,1),foo(:,2),'-');
  %axis([0 20 0 10]);
end
