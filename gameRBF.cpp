/** Program for simulating dynamic model of investment */ 
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <numeric>
#include <set>
#include <queue>
#include <string>
#include <limits>

#include <time.h>
#include <sys/time.h>
#include <omp.h>
#include<math.h>
#include<assert.h>
#include<string.h>


#ifdef _AMD
#include <amdlibm.h>
#include <acml.h>
#include <cblas.h>
#endif

#ifdef _MKL
#include "mkl.h"
#include "mkl_lapacke.h"

inline void dgels(char trans, int m, int n, int nrhs, double *a, int lda,
                  double *b, int ldb, int *info) {
  (*info) = LAPACKE_dgels(LAPACK_COL_MAJOR, trans, m, n, nrhs, a, lda,
                          b, ldb);
}
inline void dgelsy(int m, int n, int nrhs, double *a, int lda,
                   double *b, int ldb, int *jpvt, double rcond, int
                   *rank, int *info) {
  (*info) = LAPACKE_dgelsy(LAPACK_COL_MAJOR,m, n, nrhs, a, lda,
                           b, ldb, jpvt, rcond, rank);
  
}
#endif

extern "C" {
#include "random.h"
}
#include <nlopt.h>

#include "ranker.h"
#include "linalg.h" // linear algebra and other basic functions

#include "misc.hpp"
#include "kernel.hpp"
 

class Timer {
private:
  struct timeval tv;
public:
  Timer() {
    gettimeofday(&tv,NULL);
  }
  void reset() {
    gettimeofday(&tv,NULL);
  }
  double elapsed() {
    struct timeval now;
    gettimeofday(&now,NULL);
    return(now.tv_sec-tv.tv_sec+1.0e-6*(now.tv_usec-tv.tv_usec));
  }
};


/** Model parameters. */
template <class T> 
class Param
{
public: 
  vector<T> gamma; /**< coefficients of some sort u = gamma[0]*quality
                      + gamma[1]*log(y-p)  */ 
  T marketScale; ///< Size of markets (demand = marketShare * marketScale)
  vector<T> iCost; ///< investment cost 
  vector<T> psiStart; ///< investment cost initial values for estimation
  T discount; ///< discount rate
  vector<T> rhoX; ///< ar(1) coefficient for x
  T depreciation; ///< depreciation rate of capital
  double qSig; ///< std dev of shock to capital
  double tol;  ///< tolerance for computing equilibrium 
  double bwV;  ///< bandwidth for value function while computing equilibrium
  double bwS;  ///< bandwidth for policy function while computing equilibrium
  double bwEst; /**< bandwidth for estimation of policy function 
                   if <0, will use absolute value as initial value while
                   searching for bw that minimizes cv-criteria of Li
                   and Racine. This minimizes mse of f(|) 
                */
  double bwvEst; /**< bandwidth for estimation of value function */
  double bwiEst; /**< bandwidth for estimation of lhs of policy function */
  double minDensQ; /**< need density(state)>minDensQ
                      quantile of density(state) to include in
                      objective */
  double ftolAbs; ///< Convergence criteria for estimation
  double ftolRel; ///< Convergence criteria for estimation
  double xtolRel; ///< Convergence criteria for estimation
  int nExo;   ///< number of x's
  int nFirm;  ///< number of firms
  int nSolve; ///< number of points to use in value function approximation
  //int nthread; ///< number of threads
  // Estimation options
  int seed; ///< seed for random numbers
  int markets; ///< number of markets to simulate
  int time; ///< number of periods to simulate in each market
  int mcReps; ///< number of times to estimate from simulate data
  int nint; ///< number of integration points for computing expected value using monte carlo
  int maxIter; ///< maximum iterations for solving for value function
  int useSavedEq; ///< 1 if want to use saved equilibrium, 0 to resolve
  int maxMoments; ///< DEPRECATED, was maximum number of observations for use in objective function
  int useEta; ///< use true simulated eta instead of estimates
  int eqRep; /**< Number of times to solve for function. After each
                solution, the points used in the approximation are
                redrawn by simulating the policy function. */
  int nRep; /**< Number of times to repeat simulation and estimation. */
  int estimate; /**< 1 if want to estimate */
  int nCheck; /**< Number of points to check the value
                 function. Creates  some output for checking in octave */
  int plotObj; /**< Number of grid points per dimension to use to draw
                  the value. Set to zero if do not want plots. Use
                  plotObj.m to view. */
  int iObj; ///< DEPRECATED. use action based objective function 
  int vObj; ///< DEPRECATED. use value based objective function
  int fObj; ///< DEPRECATED. use first order based objective function
  int divAbs; ///< DEPRECATED. divide objective by absolute value of true
  int degree; ///< Degree of local polynomial
  int order; ///< order for kernels (must be 2 or 4)
  RANDSTATE rs; /**< random number generator state (actual sequence of
                   random numbers depends on number of threads) */  
  //string estMethod; ///< estimation method to use options are ...
  /// Default constructor
  Param() {
    // set default estimation options
    seed = 109;
    set_seed(seed,&rs);
    useSavedEq=0;
  } 
  
  /** Reads values from a file.  
      
      The format of the file is as follows.  Each line contains one
      parameter.   The line should consist of a keyword followed by a
      the parameter value(s).  The order of lines does not matter.
      Lines that do not begin with a valid keyword will be ignored.  
  */
  void valuesFromFile(const string &filename) {
    ifstream file;
    file.open(filename.c_str());
    if (!file) {
      cout << "ERROR Param::valuesFromFile: failed to open " << filename
           << endl;
      exit(1);      
    }
    string line;
    // some default values
    useSavedEq=0;
    maxMoments=-1;
    useEta = 0;
    eqRep = 10;
    nRep = 1;
    nCheck = 1000000000;
    iObj = 0;
    vObj = 1;
    fObj = 0;
    divAbs = 0;
    degree = 0;
    order = 0;
    plotObj = 0;
    minDensQ = 0.01;
    ftolAbs = 1e-12;
    ftolRel = 1e-8;
    xtolRel = 1e-6;
    while(getline(file,line)) {
      istringstream ls(line,istringstream::in);
      string name;
      ls >> name; 
      if (name=="marketScale") ls >> marketScale;
      else if (name=="depreciation") ls >> depreciation;
      else if (name=="qSig") ls >> qSig;
      else if (name=="discount") ls >> discount;
      else if (name=="nFirm") ls >> nFirm;
      else if (name=="nExo") ls >> nExo;
      else if (name=="seed") ls >> seed;
      else if (name=="markets") ls >> markets;
      else if (name=="order") ls >> order;
      else if (name=="nint") ls >> nint;
      else if (name=="nSolve") ls >> nSolve;
      else if (name=="maxIter") ls >> maxIter;
      else if (name=="tol") ls >> tol;
      else if (name=="time") ls >> time;
      else if (name=="eqRep") ls >> eqRep;
      else if (name=="nRep") ls >> nRep;
      else if (name=="bwEst") ls >> bwEst;
      else if (name=="bwvEst") ls >> bwvEst;
      else if (name=="bwiEst") ls >> bwiEst;
      else if (name=="ftolAbs") ls >> ftolAbs;
      else if (name=="ftolRel") ls >> ftolRel;
      else if (name=="xtolRel") ls >> xtolRel;
      else if (name=="minDensQ") ls >> minDensQ;
      else if (name=="bwV") ls >> bwV;
      else if (name=="bwS") ls >> bwS;
      else if (name=="useSavedEq") ls >> useSavedEq;
      else if (name=="estimate") ls >> estimate;
      else if (name=="maxMoments") ls >> maxMoments;
      else if (name=="useEta") ls >> useEta;
      else if (name=="nCheck") ls >> nCheck;
      else if (name=="plotObj") ls >> plotObj;
      else if (name=="iObj") ls >> iObj;
      else if (name=="vObj") ls >> vObj;
      else if (name=="fObj") ls >> fObj;
      else if (name=="divAbs") ls >> divAbs;
      else if (name=="degree") ls >> degree;
      else if (name=="order") ls >> order;
      else if (name=="gamma") {
        T x;
        gamma.clear();
        while (ls >> x) gamma.push_back(x);
      }
      else if (name=="iCost") {
        T x;
        iCost.clear();
        while (ls >> x) iCost.push_back(x);
      }
      else if (name=="psiStart") {
        T x;
        psiStart.clear();
        while (ls >> x) psiStart.push_back(x);
      }
      else if (name=="rhoX") {
        T x;
        rhoX.clear();
        while (ls >> x) rhoX.push_back(x);
      }
      else {
        cout << "WARNING Param::valuesFromFile: \"" << name 
             << "\" is not a valid parameter name.  Skipping this line." 
             << endl;
      }
    }
    if (seed>0) set_seed(seed,&rs);
    else set_time_seed(&rs);
    gamma.resize(nExo+1);
  } // end valuesFromFile()
  
  /** Output operator */
  friend ostream& operator<<(ostream& out, const Param<T>& pm) {
    out << "marketScale, " << pm.marketScale<< "\n"
        << "iCost, " << pm.iCost<< "\n"
        << "psiStart, " << pm.psiStart<< "\n"
        << "rhoX, " << pm.rhoX<< "\n"
        << "depreciation, " << pm.depreciation<< "\n"
        << "qSig, " << pm.qSig<< "\n"
        << "discount, " << pm.discount<< "\n"
        << "nFirm, " << pm.nFirm<< "\n"
        << "nExo, " << pm.nExo<< "\n"
        << "seed, " << pm.seed<< "\n"
        << "markets, " << pm.markets<< "\n"
        << "order, " << pm.order<< "\n"
        << "nint, " << pm.nint<< "\n"
        << "nSolve, " << pm.nSolve<< "\n"
        << "maxIter, " << pm.maxIter<< "\n"
        << "tol, " << pm.tol<< "\n"
        << "time, " << pm.time<< "\n"
        << "gamma, " << pm.gamma << "\n";      
    return(out);
  }
};

/** \brief Profits as a function of state and actions
*/
template<class T> 
class ProfitFn {
private: 
  Param<double> *theta; ///< pointer to parameters
public:
  /** \brief Constructor from void pointer. 
      
      v should be a pointer to a profitFnParam.  We define its type as
      void because OPT++ only allows void pointers to be used for
      passing auxillary parameters. 
  */
  ProfitFn() {}
  ProfitFn(Param<T> &thetaIn)
  {
    theta = &thetaIn;
  }
  /// Returns profits given state
  T operator()(const State<T> &state, T invest) {
    T profit; 
    T xg = 0;
    T sumShare = 1+exp(theta->gamma[0]*state.ownQuality);
    for(unsigned int i=0;i<state.x.size();i++) {
      xg += state.x[i]*theta->gamma[i+1];
    }
    profit = exp(theta->gamma[0]*state.ownQuality);
    for(unsigned int i=0;i<state.otherQuality.size();i++) {
      sumShare += exp(theta->gamma[0]*state.otherQuality[i]);
    }
    profit *= theta->marketScale/sumShare *(exp(xg)/(1+exp(xg)));
    
    profit -= (invest*(theta->iCost[0]-theta->iCost[1]*state.eta) 
               + theta->iCost[2]*invest*invest
               + theta->iCost[3]*((invest>0 &&  state.ownQuality>0)? invest*state.ownQuality:0.0)
               );
    //return(exp(invest)<exp(state.x)*sqrt(exp(state.ownQuality))? invest: -100);
    //return( -(state.x - invest)*(state.x-invest) - theta->gamma[0]*invest);
    return(profit);
  }
}; // end profitFn()

////////////////////////////////////////////////////////////////////////////////


/** \brief Holds a dynamic game.  
    
*/
class DynamicGame {
public:
  Param<double> param;
  ApproxFn<double> strategy; 
  ApproxFn<double> value;
  ProfitFn<double> profit;
  vector< State<double> > stateSolve; ///< points to use in solving for value function
  vector< vector<double> > eInt;
  vector< vector<double> > xInt;
  vector< vector<double> > etaInt;
  vector<double> wint; 
  int nint;
  int gridUpdate;
  int howard;
  //vector<vector<double> > Hsi; ///< hermite polynomials at stateInt
  int maxIter;
  double tol;
  DynamicGame() {} 
  /// Given param theta, constructs a dynamic game.  Initializes all
  /// fields.  
  DynamicGame(Param<double> &theta) 
  {
    param = theta;
    nint = theta.nint;
    maxIter = theta.maxIter;
    tol = theta.tol;
    profit = ProfitFn<double>(theta);
    gridUpdate = 0;
    howard = 0;

    xInt = vector<vector<double> >(nint*param.nSolve);
    eInt = vector<vector<double> >(nint*param.nSolve);
    etaInt = vector<vector<double> >(nint*param.nSolve);
    for(int i=0;i<(int) xInt.size();i++) {
      xInt[i] = vector<double>(param.nExo); 
      eInt[i] = vector<double>(param.nFirm);
      etaInt[i] = vector<double>(param.nFirm);
      for(int f=0;f<param.nFirm;f++) {
        eInt[i][f] = Sample_Normal(0,1,&param.rs);
        etaInt[i][f] = Sample_Normal(0,1,&param.rs);
      }
      for(int s=0;s<param.nExo;s++) {
        xInt[i][s] = Sample_Normal(0,1,&param.rs);
      }
    }
    
    
    strategy = ApproxFn<double>(param.bwS);
    strategy.kern.type = rbf;
    strategy.kern.order = 2;
    value = ApproxFn<double>(param.bwV);
    value.kern.type = rbf;
    value.kern.order = 2;
    stateSolve=vector<State<double> >(param.nSolve);
    vector<vector<double> > adat(stateSolve.size()); ///< data for strategy and value functions
    strategy.val = vector<double>(stateSolve.size(),0);
    value.val = strategy.val;
    for(unsigned int s=0;s<stateSolve.size();s++) {
      stateSolve[s].ownQuality = (Sample_Normal(0.5,3,&param.rs));
      stateSolve[s].otherQuality = vector<double>(param.nFirm-1);
      for(unsigned int f=0;f<stateSolve[s].otherQuality.size();f++) {
        stateSolve[s].otherQuality[f] = (Sample_Normal(0.5,3,&param.rs));
      }
      stateSolve[s].x = vector<double>(param.nExo);
      for(int f=0;f<param.nExo;f++) {
        stateSolve[s].x[f] = Sample_Normal(0,2*1/(1-param.rhoX[f]),&param.rs);
      }
      stateSolve[s].eta = Sample_Normal(0,2,&param.rs);
      adat[s] = stateSolve[s].vec();
    }
    value.updateData(adat);
    strategy.updateData(adat);
  }  

  double newQ(const State<double> &state, double invest, double shock) {
    return(state.ownQuality*(1-param.depreciation)+invest+shock);
    //double f = exp(state.x) * sqrt(exp(state.ownQuality));
    //if (f>exp(invest)) return(log(f - exp(invest)));
    //else return(-800);
    //xoreturn(state.x * sqrt(exp(state.ownQuality)) - invest);
  }
  vector<double> newX(const State<double> &state, vector<double> shock) {
    vector<double> x=state.x;
    for(unsigned int i=0;i<x.size();i++) x[i] = param.rhoX[i]*state.x[i]+shock[i];
    return(x);
  }
  
  // make stateSolve[s] be draws from the stationary distribution 
  // of states given the current strategies
  void updateSolveGrid(int T) {
    static int set = 0;
    static RANDSTATE rs0;    
    RANDSTATE rs;
    if (set==0) {
      rs0 = param.rs;
      set = 1;
    }
    rs = rs0;
    for(int t=0;t<T;t++) {
      for(unsigned int s=0;s<stateSolve.size();s++) {
        vector<double> xShock(stateSolve[s].x.size());
        State<double> ostate = stateSolve[s];
        stateSolve[s].ownQuality = this->newQ(ostate,strategy(ostate),Sample_Normal(0,1,&rs)*param.qSig);
        for(unsigned int f=0;f<ostate.otherQuality.size();f++) {
          State<double> ts = ostate;
          ts.ownQuality=ostate.otherQuality[f];
          ts.otherQuality[f]=ostate.ownQuality;
          ts.eta = Sample_Normal(0,1,&rs);
          stateSolve[s].otherQuality[f]=this->newQ(ts,strategy(ts),Sample_Normal(0,1,&rs)*param.qSig);
        }
        stateSolve[s].eta = Sample_Normal(0,1,&rs);
        for(unsigned int i=0;i<xShock.size();i++) xShock[i] = Sample_Normal(0,1,&rs);
        stateSolve[s].x = this->newX(ostate,xShock);
      }
    }
    ApproxFn<double> oldv,olds;
    oldv = value;
    olds = strategy;
    vector<vector<double> > adat(stateSolve.size());
    for(unsigned int s=0;s<stateSolve.size();s++) {
      adat[s] = stateSolve[s].vec();
      value.val[s] = oldv(stateSolve[s]);
    }
    value.updateData(adat);
    strategy.updateData(adat);
    // make approximation to strategy match A[s] on stateSolve
    { 
      double *A; 
      A = new double[stateSolve.size()];
      for(int s=0;s<(int) stateSolve.size();s++) {
        A[s] = olds(stateSolve[s]);
      }      

      int info;
      double *H, *work;
      H = new double[strategy.data.size()*stateSolve.size()];      
      work = new double[stateSolve.size()];
      for(int s=0;s<(int) stateSolve.size();s++) {
        strategy.evalWeights(work,stateSolve[s].vec());
        for(unsigned int k=0;k<stateSolve.size();k++) H[s+k*stateSolve.size()] = work[k];
      }     
      delete[] work;
      info=solveLS(stateSolve.size(), strategy.data.size(), 1, H, stateSolve.size(),
                   A, stateSolve.size()); // puts result in A on exit
      if (info) cout << "ERROR updateStrategy(): solveLS returned " << info << endl;      
      delete[] H;      
      for(unsigned int i=0;i<strategy.data.size();i++) {
        strategy.val[i] = A[i];
      }
      delete[] A;
    }
  }
  
  /// Updates value to match strategy
  void updateValue() {
    // V(s) = Pi(s) + b E[V(s')|s] 
    // v w(s) = pi(s) + b v (sum_i,k w(s'_i-s_k)w(s_i-s))
    // v(H(s) - H'(s) ) = Pi(s)
    
    // set up A, HmH and Pi
    double *HmH, *Pi; 
    Pi = new double[stateSolve.size()];
    HmH = new double[stateSolve.size()*(stateSolve.size())];

#pragma omp parallel default(shared) //private(info)
    {
      double *wi = new double[stateSolve.size()];
#pragma omp for schedule(static)     
      for(unsigned int s=0;s<stateSolve.size();s++) {
        double invest = strategy(stateSolve[s]);
        Pi[s] = profit(stateSolve[s],invest);// + param.discount*evalue(stateSolve[i],invest,i);
        value.evalWeights(wi,stateSolve[s].vec());
        for(unsigned int k=0;k<stateSolve.size();k++) HmH[s+k*stateSolve.size()] = wi[k];
        //cout << H << endl;
        for(int nn=0;nn<nint;nn++) {
          int n = nn+s*nint;
          State<double> stateNew=stateSolve[s];
          double p=1;
          stateNew.ownQuality = this->newQ(stateSolve[s],strategy(stateSolve[s]),eInt[n][0]*param.qSig);
          for(unsigned int f=0;f<stateNew.otherQuality.size();f++) {
            State<double> ostate = stateSolve[s];
            double q = ostate.ownQuality;
            ostate.ownQuality = ostate.otherQuality[f];
            ostate.otherQuality[f] = q;
            ostate.eta = etaInt[n][f+1];
            stateNew.otherQuality[f] = this->newQ(stateSolve[s],strategy(stateSolve[s]),eInt[n][f+1]*param.qSig);
          }
          stateNew.x = newX(stateSolve[s],xInt[n]);
          stateNew.eta = etaInt[n][0];
          value.evalWeights(wi,stateNew.vec());
          for(unsigned int k=0;k<value.data.size();k++) {
            HmH[s+k*stateSolve.size()] -= param.discount*p/nint*wi[k];
          }
        } // n
      }
      delete[] wi;
    }
    // end parallel region
    // Solve HmH*A ~= Pi 
    int info=solveLS(stateSolve.size(),stateSolve.size(), 1, HmH, stateSolve.size(), 
                     Pi, stateSolve.size()); // puts result in Pi on exit
    if (info) cout << "ERROR updateValue(): solveLS returned " << info << endl;
    for(unsigned int i=0;i<value.val.size();i++) value.val[i] = Pi[i];
    delete[] HmH;
    delete[] Pi;
    /*
    for(unsigned int s=0;s<10;s++) {
      double val;
      double invest = strategy(stateSolve[s]);
      val = profit(stateSolve[s],invest) + param.discount*evalue(stateSolve[s],invest,s);
      printf("%2d: pi+b*Ev = %6.3g | v = %6.3g\n",
             s,val, value(stateSolve[s]));
    }
    */
  } 

    
  double evalue(const State<double> &state, const double invest,const int offset) {
    double val=0;
    for(int nn=0;nn<nint;nn++) {
      int n = nn+offset*nint;
      State<double> stateNew=state;
      double p=1;
      stateNew.ownQuality = newQ(state,invest,eInt[n][0]*param.qSig);
      for(unsigned int f=0;f<stateNew.otherQuality.size();f++) {
        State<double> ostate = state;
        double q = ostate.ownQuality;
        ostate.ownQuality = ostate.otherQuality[f];
        ostate.otherQuality[f] = q;
        ostate.eta = etaInt[n][f+1];
        stateNew.otherQuality[f] = newQ(ostate,strategy(ostate),eInt[n][f+1]*param.qSig);
      }
      stateNew.x = newX(state,xInt[n]);
      stateNew.eta = etaInt[n][0];
      val += value(stateNew)*p;
      //cout << "nn=" << value.neighbor << " " << invest << " state =" << stateNew.vec() << endl;
    }
    val /= nint;
    return(val);
  }
  double vTilde(const double &a, const int i) {
    double prof = profit(stateSolve[i],a);
    double ev = evalue(stateSolve[i],a,i);
    if (prof>20) printf("waring prof=%6.3g, i=%d, a=%6.3g\n",prof,i,a);
    if (ev>20) printf("waring ev=%6.3g, i=%d, a=%6.3g\n",ev,i,a);
    return(prof + param.discount*ev);
  }
  
  // given current value and strategy approximations, find 
  //  argmax_a Pi(s,a) + E[V(s')|s,a]
  double bestAction(State<double> &state,const int si) {
    double alo, ahi, a, am1, am2;
    double plo, phi, p;
    const double amin = -10;
    const double amax = 10;
    const double da = 0.1;
    const double atol = 1e-4;
    const double gr = 0.38197;
    int it=0, itint=0, itsec=0;
    // try initial brackets around current strategy
    a = strategy(state);
    p = profit(state,a) + param.discount*evalue(state,a,si);
    alo=a-da;
    if (alo<amin) alo=amin;
    ahi=a+da;
    plo = profit(state,alo) + param.discount*evalue(state,alo,si);
    phi = profit(state,ahi) + param.discount*evalue(state,ahi,si);
    if (plo>p || phi>p) { // this bracket failed
      // construct initial brackets using grid search
      p = -1e300;
      a = 0.5*(amax+amin);
      for(double atry=amin;atry<=amax;atry+=da) {
        double ptry = profit(state,atry)+param.discount*evalue(state,atry,si); 
        if (ptry>p) {
          p = ptry; a = atry;
        }
      }
      //if (a==amin) cout << "WARNING: atry==amin\n";
      //else if (a==amax) cout << "WARNING: atry==amax\n";
      alo = a-da;
      ahi = a+da;
      plo = profit(state,alo) + param.discount*evalue(state,alo,si);
      phi = profit(state,ahi) + param.discount*evalue(state,ahi,si);
    }
    am1 = a - 1;
    am2 = a - 2;
    // use brent's method to find maximum
    while ((ahi - alo)>atol) {
      int needsection=0;
      double atry, ptry;
      // parabolic interpolation
      double den = (plo-p)*(ahi-a)+(phi-p)*(a-alo);
      if (den!=0 && (a !=
                     (atry = a + 0.5*( (plo-p)*(ahi-a)*(ahi-a)+(phi-p)*(a-alo)*(a-alo) ) / den) )
          && atry>alo && atry<ahi && fabs(am1-am2)>1.6*atol ) {
        //fabs(atry-a)<(1-gr)*fabs(am1-am2)) {
        ptry = profit(state,atry)+param.discount*evalue(state,atry,si);
        itint++;  
        //cout << "quad interp " << atry << " " << ptry << endl;
        if (ptry>p) {
          am2 = am1; am1 = a;
          if (atry<a) {
            //cout << " atry<a ";
            ahi = a; phi = p;
          } else {
            //cout << " atry>=a ";
            alo = a; plo =p;
          }
          a = atry; p = ptry;
        } else { // parabola did not improve anything
          needsection = 1;
        }
      } else needsection=1;
      if (itint>5 && itint>itsec) needsection=1;
      // section search
      if (needsection) {
        if ((ahi-a) > (a-alo) ) {
          atry = a + (ahi-a)*gr;
          ptry = profit(state,atry) + param.discount*evalue(state,atry,si);
          am2 = am1; am1 = a;
          if (ptry>p) {
            alo=a; plo=p;
            a=atry; p=ptry;
          } else {
            ahi = atry; phi=ptry;
          }
        } else {
          atry = a + (alo-a)*gr;
          ptry = profit(state,atry) + param.discount*evalue(state,atry,si);
          am2 = am1; am1 = a;
          if (ptry>p) {
            ahi=a; phi=p;
            a=atry; p=ptry;
          } else {
            alo = atry; plo=ptry;
          }
        }
        itsec++;
      }
      it++;
      //cout << it << ": " << ahi-alo << " itint=" << itint << " itsec=" << itsec<< endl;
      if (it>5000) {
        cout << "Too many iterations " << ahi-alo << " itint=" << itint << " itsec=" << itsec << endl;
        break;
      }
    }
    //cout << "a=" << a << " p=" << p << " it=" << it << " itsec " << itsec << " itint " << itint << endl;
    //  " a*=" << state.x - param.gamma[0]/2 << " p*=" << profit(state,state.x-param.gamma[0]/2) 
    //     << " strat=" << strategy(state)
    //     <<endl;
    return(a);
  }

  /// 
  void updateStrategy() {
    double *A; //, *H;
    //int info;
    A = new double[stateSolve.size()];
    //
#pragma omp parallel default(shared) //private(info)
    {
#pragma omp for schedule(static)     
      for(int s=0;s<(int) stateSolve.size();s++) {
        A[s] = bestAction(stateSolve[s],s);
      }      
    } // end parallel block
    if (1) { 
      // make approximation to strategy match A[s] on stateSolve
      int info;
      double *H, *work;

#ifndef NDEBUG
      FILE *out = fopen("strat.csv","w");
#endif
      
      H = new double[strategy.data.size()*stateSolve.size()];      
      work = new double[stateSolve.size()];
      for(int s=0;s<(int) stateSolve.size();s++) {
        strategy.evalWeights(work,stateSolve[s].vec());
        for(unsigned int k=0;k<stateSolve.size();k++) H[s+k*stateSolve.size()] = work[k];
      }     
      delete[] work;
#ifndef NDEBUG      
      for(int s=0;s<(int) stateSolve.size();s++) {
        if (s<20) printf("before: %2d: %6.3g\n",s,A[s]);
        fprintf(out,"%.16g ",A[s]);
        for(unsigned int k=0;k<stateSolve.size();k++) fprintf(out," , %.16g",H[s+k*stateSolve.size()]);
        fprintf(out,"\n");
      }
#endif
      
      info=solveLS(stateSolve.size(), strategy.data.size(), 1, H, stateSolve.size(),
                   A, stateSolve.size()); // puts result in A on exit
      if (info) cout << "ERROR updateStrategy(): solveLS returned " << info << endl;      
      delete[] H;      
      for(unsigned int i=0;i<strategy.data.size();i++) {
        strategy.val[i] = A[i];
#ifndef NDEBUG
        if (i<20) printf(" after: %2d: %6.3g %6.3g\n",i,A[i],strategy(stateSolve[i]));
        fprintf(out,"%.16g, ",A[i]);
#endif
      }
#ifndef NDEBUG
      fprintf(out,"\n");
      fclose(out);
      cout << "enter int to continue" << endl;
      cin >> info;
#endif
    }  
    else { // just use kernel regression A[s] to approx strategy
      for(unsigned int i=0;i<strategy.data.size();i++) {
        strategy.val[i] = A[i];
        //  fprintf(out,"%.16g, ",A[i]);
      }
    }
    delete[] A;
  }
    

  /** Computes the equilibrium of the game.  
   */
  void computeEquilibrium()
  {
    double normV, normS, normV2=100, normS2=100;
    int iter=0;
    normV = normS = 100;
    ApproxFn<double> stratGrid = strategy;
    double lastDV = 0;
    /*
      for(int i = 0;i<strategy.coef.size();i++) {
      int match = 1;
      int J = strategy.powers[i].size()-1;
      for(int j=0;j<J && match ;j++) 
        match = match && (strategy.powers[i][j]==0);
      if (match && (strategy.powers[i][J]==1)) strategy.coef[i] = -1; 
      else if (match && (strategy.powers[i][J]==0)) strategy.coef[i] = 2; 
      else strategy.coef[i] = 0;
    }
    */
    updateValue();
    while(normV+normS2+normV2>tol) {
      ApproxFn<double> oldV = value;
      ApproxFn<double> oldS = strategy;
      updateStrategy();
      //cout << "Enter a number to continue.\n";
      //cin >> t;
      updateValue();
      normV = rhol2(value.val,oldV.val);
      normV *= normV;
      normS = rhol2(strategy.val,oldS.val);
      normS *= normS;      
      normV /= value.val.size();
      normS /= strategy.val.size();      
      normV2 = normS2 = 0;
      for(unsigned int i=0;i<stateSolve.size();i++) {
        double d = value(stateSolve[i]) - oldV(stateSolve[i]);
        normV2 += d*d;
        d = strategy(stateSolve[i]) - oldS(stateSolve[i]);
        normS2 += d*d;
      }
      normV2 /= stateSolve.size();
      normS2 /= stateSolve.size();      

      iter++;
      cout << iter << ": ||dv||=" << normV << "," << normV2 << endl;
      //cout << oldV.coef << endl;      
      cout << " ||ds||="<<normS << "," << normS2 << endl;
      printf("%d calls to value, %d nn used\n",value.calls, value.neighbor);
      printf("%d calls to strategy, %d nn strategy\n",strategy.calls, strategy.neighbor);
      //cout << oldS.coef << endl << "-----------------------------------------" << endl;            
      if (iter>=maxIter) {
        cout << "Too many iterations: ||dv||=" << normV << " ||ds||="<<normS << endl;
        return;
      }
      if (gridUpdate && rhol2(strategy.val,stratGrid.val)>strategy.val.size()*0.2) {
        int T = 10<iter? 10:iter;
        cout << "iteration " << iter << " updated solve grid, ds = " 
             << rhol2(strategy.val,stratGrid.val) << endl;
        updateSolveGrid(T);
        stratGrid = strategy;
      }
      //updateSolveGrid(1);  
      if (fabs(lastDV - normV)<1e-8) {
        //updateSolveGrid(1);  
        stratGrid = strategy;      
      }
      lastDV = normV;
      //if ((iter && iter%10==0 && howard) || (!howard && normS<tol)) howard = !howard;
    }
    cout << "Convergence: ||dv||=" << normV << " ||ds||="<<normS << endl;
    //cout << "value = " << endl << value << endl;
    //cout << "strategy = " << endl << strategy << endl;

  } // end computeEquilibrium()

  void save(const string &filename) {
    fstream fout; 
    fout.open(filename.c_str(),fstream::out);
    for(unsigned int s=0;s<value.val.size();s++) {
      fout << value.val[s] << " , " << strategy.val[s];
      for(unsigned int k=0;k<value.data[s].size();k++) {
        fout << " , " << value.data[s][k];
      }
      fout << endl;
    }
    fout.close();
  }
  void load(const string &filename) {
    ifstream fin;  
    string sline;
    int nline = 0;
    fin.open(filename.c_str());
    if (!fin.is_open()) {
      cout << "ERROR (dynamicGame::load) Failed to open " << filename << endl;
      exit(-1);
    }
    value.val.clear();
    value.data.clear();
    strategy.val.clear();
    strategy.data.clear();
    while(getline(fin,sline)) {
      istringstream line(sline,istringstream::in);
      string comma;
      double v,s;
      vector<double> data;
      data.clear();
      line >> v;
      line >> comma;
      line >> s;
      while (line >> comma) {
        double d;
        line >> d;
        data.push_back(d);
      }
      if (value.data.size()>0 &&
          value.data[0].size() != data.size()) {
        cout << "ERROR reading " << filename << endl;
      }
      value.val.push_back(v);
      strategy.val.push_back(s);
      value.data.push_back(data);      
      stateSolve[nline++].unvec(data); 
    }
    strategy.updateData(value.data);
    value.updateData(value.data);
    fin.close();
  }
}; // end class dynamicGame


double eqFunc(int n, const double *x, double *grad, void *vptr) {
  DynamicGame *dg = (DynamicGame *) vptr;
  double fn=0;
  if (n!= (int) (dg->value.val.size()+dg->strategy.val.size())) {
    cout << "ERROR eqfunc() x wrong size\n";
    exit(1);
  }
  unsigned int i=0;
  for(unsigned int j=0;j<dg->value.val.size();j++) dg->value.val[j] = x[i++];
  for(unsigned int j=0;j<dg->strategy.val.size();j++) dg->strategy.val[j] = x[i++];
  // optimality errors
  const double da = 0.05;
#pragma omp parallel default(shared) 
  {
#pragma omp for schedule(static) reduction(+ : fn)
    for(int i=0;i<(int) dg->strategy.val.size();i++) {
      double p0,p1, a = dg->strategy.val[i];
      p0 = dg->vTilde(a,i);
      p1 = dg->vTilde(a-da,i);
      if (p1>p0) fn = fn+(p1-p0)*(p1-p0);
      p1 = dg->vTilde(a+da,i);
      if (p1>p0) fn = fn+(p1-p0)*(p1-p0);
    }
  }
  printf("eqFunc opt= %g\n",fn);
  // bellman equation errors
  dg->updateValue();
  for(unsigned int i=0;i<dg->value.val.size();i++) {
    double d= x[i]-dg->value.val[i];
    fn += d*d;
  }
  printf("eqFunc tot = %g\n",fn);
  return(fn);
}

/*** 
     Checks the quality of the computed equilibrium by computing best 
     responses off of solve grid and recording average error in action
     and value.
*/
void checkEq(DynamicGame &dg, const int nCheck) {
  DynamicGame copy = dg;
  double ma=0,maa=0,msa=0,mv=0,mav=0,msv=0;
  int N = nCheck < (int) dg.stateSolve.size()? nCheck:dg.stateSolve.size();
  vector<double> A(N),V(N);
  // perturb solve grid by simulating 1 period
  copy.updateSolveGrid(1);
#pragma omp parallel default(shared)
  {
    //fstream fout("strat.csv",fstream::out);
    //fstream sout("state.csv",fstream::out);
#pragma omp for schedule(static)     
    for(int s=0;s<N;s++) {
      A[s] = dg.bestAction(copy.stateSolve[s],s);
      V[s] = dg.profit(copy.stateSolve[s],A[s])+dg.param.discount*dg.evalue(copy.stateSolve[s],A[s],s);
    }
  }
  for(int s=0;s<N;s++) {
    double a = (A[s]-dg.strategy(copy.stateSolve[s]));
    double v = (V[s]-dg.value(copy.stateSolve[s]));
    ma += a; msa += a*a; maa += fabs(a);
    mv += v; msv += v*v; mav += fabs(v);
  }
  ma /= A.size(); msa/=A.size(); maa/=A.size();
  mv /= V.size(); msv/=V.size(); mav/=V.size();
  printf("average policy error= %.3g, mse=%.3g, mae=%.3g\n",
         ma,msa,maa);
  printf("average value error= %.3g, mse=%.3g, mae=%.3g\n",
         mv,msv,mav);
  FILE *out=fopen("func.csv","w");
  for(int s=0;s<N;s++) {
    fprintf(out,"%.8g,%.8g,%.8g,%.8g,%.8g",dg.strategy(copy.stateSolve[s]),A[s],
            dg.value(copy.stateSolve[s]),V[s],dg.strategy.mu[s]);
    for(int j=0;j<(int) copy.value.data[s].size();j++) {
      fprintf(out,",%.8g",copy.value.data[s][j]);
    }
    fprintf(out,"\n");
  }
  fclose(out);
}

class cvStepAhead {
public:
  DynamicGame *dg;
  DynamicGame dgcp;
  vector<double> A,V;
  int valuecv;
  cvStepAhead(DynamicGame *in) {
    dg = in;
    dgcp = *dg;
    dgcp.updateSolveGrid(1);
    dgcp.value = dg->value;
    dgcp.strategy = dg->strategy;
    valuecv = 0;
    A = vector<double>(dgcp.stateSolve.size());
    V = vector<double>(dgcp.stateSolve.size());
#pragma omp parallel default(shared)
    {
      //fstream fout("strat.csv",fstream::out);
      //fstream sout("state.csv",fstream::out);
#pragma omp for schedule(static)     
      for(int s=0;s<(int) dgcp.stateSolve.size();s++) {
        A[s] = dgcp.bestAction(dgcp.stateSolve[s],s);
        V[s] = dgcp.profit(dgcp.stateSolve[s],A[s])+dg->param.discount*dg->evalue(dgcp.stateSolve[s],A[s],s);
      }
    }
  }
  double mse(const double bw) {
    double mse = 0;
    if (valuecv) {
      double bwsave = dg->value.bw;
      dg->value.bw = bw;
      for(unsigned int s=0;s< dgcp.stateSolve.size();s++) {
        double dv = (V[s] - dg->value(dgcp.stateSolve[s]));
        mse += dv*dv;
      }
      dg->value.bw = bwsave;
    } else {
      double bwsave = dgcp.strategy.bw;
      dgcp.strategy.bw = bw;
      for(unsigned int s=0;s< dgcp.stateSolve.size();s++) {
        double dv = (A[s] - dg->strategy(dgcp.stateSolve[s]));
        mse += dv*dv;
      }
      dg->strategy.bw = bwsave;
    }   
    return(mse);
  }
};

double cvObjStep(int n, const double *x, double *grad, void *vp)
{
  grad = NULL;
  return(((cvStepAhead *) vp)->mse(x[0]));
}

////////////////////////////////////////////////////////////////////////////////

class MarketState {
public: 
  vector<double> quality;
  vector<double> x;
  vector<double> invest;
  vector<double> eta;
  vector<double> revenue;
  MarketState() {}
  MarketState(const State<double> &fs) {
    this->quality = fs.otherQuality;
    this->quality.push_back(fs.ownQuality);
    this->x = fs.x;
    this->invest = vector<double>(this->quality.size());
    this->revenue = vector<double>(this->quality.size());
    this->eta = this->invest;
  }
  /** 
      takes this state, draws shocks, fills in invest, draws transitions, 
      and returns next state
  */
  MarketState simulate(Param<double> *param, DynamicGame *game,RANDSTATE *rs) {
    MarketState next = (*this);
    vector<double> xShock(param->nExo,0);
    State<double> fs;
    fs.x = this->x;
    fs.otherQuality = vector<double>(quality.size()-1);
    for(unsigned int f=0;f<quality.size();f++) {      
      int runi; 
      double newQshock;
      fs.ownQuality = quality[f];
      for(unsigned int f2=0;f2<quality.size();f2++) {      
        if (f2<f) fs.otherQuality[f2]=quality[f2];
        else if (f2>f) fs.otherQuality[f2-1]=quality[f2];
      }
      //#pragma omp critical 
      {
        runi = (int) Sample_Uniform(0,1,rs)*game->stateSolve.size();
        fs.eta = eta[f] = Sample_Normal(0,1,rs);;
        newQshock = Sample_Normal(0,1,rs)*param->qSig;
      }
      this->invest[f] = //game->strategy(fs);
        game->bestAction(fs, runi);
      this->revenue[f] = game->profit(fs,0.0);
      next.quality[f] = game->newQ(fs,this->invest[f],newQshock);
    }
    //#pragma omp critical 
    {
      for(unsigned int i=0;i<xShock.size();i++) xShock[i] = Sample_Normal(0,1,rs);
    }
    next.x = game->newX(fs,xShock);
    return(next);
  }
};

State<double> getFirmState(const MarketState &ms, const unsigned int f) {
  State<double> fs;
  fs.x = ms.x;
  fs.otherQuality = vector<double>(ms.quality.size()-1);
  fs.ownQuality = ms.quality[f];
  for(unsigned int f2=0;f2<ms.quality.size();f2++) {      
    if (f2<f) fs.otherQuality[f2]=ms.quality[f2];
    else if (f2>f) fs.otherQuality[f2-1]=ms.quality[f2];
  }
  fs.eta = ms.eta[f];
  return(fs);
}

////////////////////////////////////////////////////////////////////////////////

class MarketData {
public: 
  Param<double> *param;
  int index;
  vector<MarketState> state; ///< T elements with state each period
  MarketData() { }
};

#define MAXTHREADS 100 ///< maximum number of threads, changing this
                       ///will alter the sequence of random numbers
                       ///even for the same seed.

class Data {
public: 
  Param<double> *param;
  DynamicGame *game;
  vector<MarketData> market; 
  Data() { param = NULL; }
  Data(Param<double> *pin, DynamicGame *gin) { 
    param = pin; 
    game = gin;
  }
  void simulate() {
    unsigned int seed[MAXTHREADS]; 
    if (!param) {
      cout << "ERROR simulate(): param not set" << endl;
      exit(1);
    }
    for (int i=0;i<MAXTHREADS;i++) {
      seed[i] = (unsigned int)
        (Sample_Uniform(0,1,&(param->rs)) * 
         numeric_limits<unsigned int>::max());
    }
    market = vector<MarketData>(param->markets);
    cout << "WARNING simulate() assumes game has already been solved" << endl;
#pragma omp parallel default(shared) 
    {
      RANDSTATE rs;      
      if (omp_get_num_threads()>MAXTHREADS) {
        printf("ERROR: number of threads, %d, exceeds MAXTHREAD=%d\n"
               " Can increase MAXTHREAD and recompile, but this will change\n"
               " the sequence of random numbers, even for the same seed.\n",
               omp_get_num_threads(),MAXTHREADS);
        exit(omp_get_num_threads());
      } 
      set_seed(seed[omp_get_thread_num()],&rs);
#pragma omp for 
      for(unsigned int m=0;m<market.size();m++) {
        int runi;
        //#pragma omp critical 
        {
          runi = (int) (Sample_Uniform(0,game->stateSolve.size(),&rs));
          // choose randomly from solve grid for initial state
          //runi = m/(market.size()/4);
        }
        MarketState current(game->stateSolve[runi]);   
        market[m].state = vector<MarketState>(param->time);
        for(int t=0;t<param->time;t++) {
          MarketState next = current.simulate(param,game,&rs);
          if (t>=0) market[m].state[t] = current;
          current = next;
        }
        //if (m%10==0) 
#pragma omp critical 
        {
          cout << "simulated " << m << " of " << market.size() << " markets." << endl;
        }
      }
    } 
  } // end parallel region
  
};


////////////////////////////////////////////////////////////////////////////////

#define KSIZE 5 // pi = 1 psi[0] psi[1] psi[2] psi[3]


class Estimation {
private:
  double *pivec, *H;
  double *K;  // Profit(data) = K * pivec
  double *E;  // Expectation operator
  double *dK; // d Profit / di = dK * pivec
  double *dE; // discount dE/di
  double *dKpdEHK; 
  double *EV;
  double *dEV;
  double *HK;
public:
  int initialized;
  Param<double> param;
  Data *data;
  ApproxFn<double> Vhat;
  ApproxFn<double> eVhat;
  ApproxFn<double> invest;
  int nI, nV, nE;
  vector<vector<vector<double> > > eta;
  vector<int> ltp1;
  vector<int> invl;
  vector<vector<double> > exWeight; ///< weights for computing expectations
  vector< vector<double> > sivec; ///< state vector with invest instead of eta since 
                                  // invest is observed and the info for firm is equivalent
  vector<double> stateDens; ///< kernel density of sivec
  double minDens;

  void getmtf(unsigned int i,unsigned int &m,unsigned int &t,unsigned int &f) {
    unsigned int T = data->market[0].state.size();
    unsigned int F = data->market[0].state[0].quality.size();
    m = i/(T*F);
    i = i%(T*F);
    t = i/(F);
    f = i%F;
  }
  unsigned int geti(unsigned int m,unsigned int t,unsigned int f) {
    unsigned int T = data->market[0].state.size();
    unsigned int F = data->market[0].state[0].quality.size();
    return(m*(T*F)+t*F+f);
  }
  
  Estimation() { 
    initialized=0; 
    // set pointers to NULL 
    pivec=H=K=E=dK=dE=dKpdEHK=EV=dEV=HK=NULL;
  }
  
  void setParam(const double *x) {
    unsigned int i=0;
    //for(i=0;i<param.gamma.size();i++) param.gamma[i] = x[i];
    for(unsigned int j=0;j<param.iCost.size();j++) param.iCost[j] = x[i++];
  }
  

  /** 
      Estimate eta=n from 
        Phi(n) = F(i|x)
        n = Phi^-1 [F(i|x)]
      eta is indexed by [markets][time][firms]        

      Use observed state,invest,and estimated eta to form kernel reg estimate of policy function
  */
  void estimateEtaAndPolicy()
  {
    ApproxFn<double> Fi(param.bwEst);
    vector<double> ivec;
    Fi.kern.type = epan;
    Fi.kern.order= param.order;
    Fi.degree = 0; //param.degree;
    eta.resize(data->market.size());
    // extract data for kernel regressions to get eta
    for(unsigned int m=0;m<data->market.size();m++) {
      eta[m].resize(data->market[m].state.size());
      for(unsigned int t=0;t<data->market[m].state.size();t++) {
        eta[m][t].resize(data->market[m].state[t].quality.size());
        for(unsigned int f=0;f<eta[m][t].size();f++) {
          vector<double> dat;
          State<double> fs = getFirmState(data->market[m].state[t],f);
          ivec.push_back(data->market[m].state[t].invest[f]);
          dat.push_back(fs.ownQuality);
          for(unsigned int i=0;i<fs.otherQuality.size();i++) dat.push_back(fs.otherQuality[i]);
          for(unsigned int j=0;j<fs.x.size();j++) dat.push_back(fs.x[j]);
          Fi.data.push_back(dat);
        }
      }
    }
    Fi.updateData(Fi.data);
    Fi.val = vector<double>(Fi.data.size());
    Fi.val = ivec;
    double bwi;
    if (param.bwEst<0) // search for bandwidth that minimizes mse of f(|). 
    {
      double etaMse(int n, const double *x, double *grad, void *appFn);
      param.bwEst = -param.bwEst;
      double x[2], xhi[2], xlo[2], cv;
      void *etaMseArgs[3];
      x[0] = param.bwEst; x[1] = param.bwiEst;
      printf("bw=%g bwi=%g\n",x[0], x[1]);
      xlo[0] = 1e-3; xlo[1] = 1e-6;
      xhi[0] = 20; xhi[1] = 20;
      cout << "bandwidth search start" << endl;
      etaMseArgs[0] = this; 
      etaMseArgs[1] = &Fi;
      etaMseArgs[2] = &ivec;
      int returnCode = // NLOPT_LN_COBYLA
        nlopt_minimize_constrained(NLOPT_GN_DIRECT_L, 2, etaMse, //    cvObjective, (void*) &Fi
                                   etaMseArgs,
                                   0, NULL, NULL,0,
                                   xlo, xhi, x, &cv,
                                   -HUGE_VAL, 1e-8, 1e-12, 1e-4, NULL, 1000, 0);
      /*                           ftol_rel, ftol_abs, xtol_rel, xtol_abs=NULL */ 
      param.bwEst = x[0]; 
      bwi= x[1];
      printf("code = %d, bw = %g, bwi = %g, cv=%g\n",returnCode,param.bwEst,bwi,cv);
      printf("etaMse=%.2g\n",etaMse(1,x,NULL,etaMseArgs));
      param.bwiEst = bwi;
      nloptReturnCodeMessage(returnCode);
    } else {
      bwi = param.bwiEst;       
      printf("Using bw=%g, bwi=%g\n",param.bwEst,bwi);
    }
    invest.bw = param.bwEst;
    Fi.bw = param.bwEst;
    Fi.degree = 0; //param.degree;
    invest.val=ivec;    
    invest.kern.order = param.order;
    invest.degree = param.degree;
    invest.kern.type = epan;
    invest.data.clear();
    // do kernel regressions to estimate eta
    invest.data = vector<vector <double> >(Fi.data.size());
#pragma omp parallel default(shared) 
    {
      ApproxFn<double> FiCopy = Fi;
#pragma omp for 
      for(unsigned int m=0;m<data->market.size();m++) {
        for(unsigned int t=0;t<data->market[m].state.size();t++) {
          for(unsigned int f=0;f<eta[m][t].size();f++) {
            State<double> fs = getFirmState(data->market[m].state[t],f);
            for(unsigned int i=0;i<ivec.size();i++) 
              FiCopy.val[i] = FiCopy.kern.integrate(data->market[m].state[t].invest[f],ivec[i],param.bwiEst);
            vector<double> dat;
            dat.clear();
            dat.push_back(fs.ownQuality);


            for(unsigned int i=0;i<fs.otherQuality.size();i++) dat.push_back(fs.otherQuality[i]);
            for(unsigned int j=0;j<fs.x.size();j++) dat.push_back(fs.x[j]);
            fs.eta = eta[m][t][f] = invnormcdf(FiCopy(dat),0,1);
            if (param.useEta) fs.eta = eta[m][t][f] = data->market[m].state[t].eta[f];
            invest.data[geti(m,t,f)] = fs.vec();
            if (geti(m,t,f) < 20) {
              printf("%d: etaHat=%.2g eta=%.2g\n",geti(m,t,f),eta[m][t][f],data->market[m].state[t].eta[f]);
            } // fi
          } // for(i)
        } // for(t)
      } // for(m)
    } // end parallel region
    invest.updateData(invest.data);
    nI++;
  }


void estimateTransitionDensity()
  {
    Kernel kern;
    ApproxFn<double> fn;
    int c = 0;    
    unsigned int N = sivec.size();
    sivec.clear();
    kern.order= param.order;
    ltp1 = vector<int>(invest.val.size(),-1);
    invl = ltp1;
    // fill in sivec, ltp1, and invl
    // -sivec is number of observations by size of state.vec()
    //   it is the same as state.vec(), but with eta replaced by invest 
    // -ltp1[j] = location in sivec of observation for same firm at
    //   time t+1, it is < 0 if there is no such observation
    // -invl[j] = location in sivec of observation for same firm at
    //   time t-1, it is < 0 if there is no such observation    
    for(unsigned int m=0;m<data->market.size();m++) {
      for(unsigned int t=0;t<data->market[m].state.size();t++) {
        for(unsigned int f=0;f<data->market[m].state[t].quality.size();f++) {
          State<double> fs = getFirmState(data->market[m].state[t],f);
          vector<double> v ;
          v.push_back(fs.ownQuality); //This include other quality which is redundent
          v.push_back(data->market[m].state[t].invest[f]); 
          //v comprise of otherQuality, ownQuality(x) and eta. 
          //Replace eta with invest
          sivec.push_back(v);
          if (t<data->market[m].state.size()-1) {
            ltp1[c] = c + data->market[m].state[t].quality.size();            
            invl[ltp1[c]] = c;
          }
          c++;
        }
      }
    }
    fn.kern.order = kern.order;
    fn.degree = param.degree;
    fn.bw = param.bwvEst;
    fn.updateData(sivec);
    ApproxFn<double> efn = fn;

    vector<int> efnIndex(0);
    for(int j=efn.data.size()-1;j>=0;j--) {
      if (!(ltp1[j]>=0 && ltp1[j]<(int) sivec.size())) {
        efn.data.erase(efn.data.begin()+j);
        efn.mu.erase(efn.mu.begin()+j);
      } else {
        efnIndex.push_back(j);

      }
    }
    reverse(efnIndex.begin(),efnIndex.end());
    //efnIndex is the time periods that have t+1 and t-1 
    efn.updateData(efn.data);

    //Transition Density of (x,i)
    //Not sure if this is right
    ApproxFn<double> transition; //Takes (x_t,i_t,x_t-1,i_t-1) returns a probability
    transition.kern.order = kern.order;
    transition.degree = param.degree;
    transition.bw  = param.bwvEst;
    transition.data = vector<vector <double> >(sivec.size()*efnIndex.size());
    transition.val = vector<double>(sivec.size()*efnIndex.size());
    //End of the uncertain part
    exWeight = vector<vector<double> >(sivec.size());


    for(unsigned int i=0;i<sivec.size();i++) {
      exWeight[i] = vector<double>(sivec.size(),0); 
    }

    // for(unsigned int i=0;i<transition.data.size();i++){
    //   transition.data[i] = vector<double>(4,0);
    // }

    unsigned int ii = 0;  
    for(unsigned int i=0;i<sivec.size();i++) {
      double *xwx=NULL, *xw=NULL;
      unsigned int nx,n;
      efn.getxwx(&xwx,&xw,nx,n, sivec[i], 1);
      int info;
      dgels('N',nx,nx,n,xwx,nx,xw,nx,&info);
      for (unsigned int j=0;j<efn.data.size();j++) {
        exWeight[i][efnIndex[j]] = xw[0 + j*nx];
        transition.data[ii] = vector< double >(4,0) ;
        unsigned int kk = 0;
        for (unsigned int jj = 0; jj<sivec[i].size();jj++){
          transition.data[ii][kk] = sivec[i][jj] ;
          kk ++ ;
        }
        for (unsigned int jj = 0; jj<sivec[efnIndex[j]].size();jj++){
          transition.data[ii][kk] = sivec[efnIndex[j]][jj] ;
          kk ++ ;
        }
        transition.val[ii] = xw[0+j*nx];
        ii++;
      }
      delete[] xwx;
      delete[] xw;
    }  
  }



  void computeExWeight() {
    Kernel kern;
    ApproxFn<double> fn;
    int c = 0;
    sivec.clear();
    kern.order= param.order;
    ltp1 = vector<int>(invest.val.size(),-1);
    invl = ltp1;
    // fill in sivec, ltp1, and invl
    // -sivec is number of observations by size of state.vec()
    //   it is the same as state.vec(), but with eta replaced by invest 
    // -ltp1[j] = location in sivec of observation for same firm at
    //   time t+1, it is < 0 if there is no such observation
    // -invl[j] = location in sivec of observation for same firm at
    //   time t-1, it is < 0 if there is no such observation    
    for(unsigned int m=0;m<data->market.size();m++) {
      for(unsigned int t=0;t<data->market[m].state.size();t++) {
        for(unsigned int f=0;f<data->market[m].state[t].quality.size();f++) {
          State<double> fs = getFirmState(data->market[m].state[t],f);
          vector<double> v = fs.vec();


          v[v.size()-1] = data->market[m].state[t].invest[f];
          sivec.push_back(v);
          if (t<data->market[m].state.size()-1) {
            ltp1[c] = c + data->market[m].state[t].quality.size();            
            invl[ltp1[c]] = c;            
          }
#ifndef NDEBUG
          {
            unsigned int mm,tt,ff;
            // test of geti and getmtf
            assert(c==(int) geti(m,t,f));
            getmtf(c,mm,tt,ff);
            assert(m==mm);
            assert(t==tt);
            assert(f==ff);
            if (ltp1[c]>=0) {
              getmtf(ltp1[c],mm,tt,ff);
              assert(m==mm);
              assert(t+1==tt);
              assert(f==ff);
            }
          }
#endif
          c++;
        }
      }
    }
    exWeight = vector<vector<double> >(sivec.size());
    fn.kern.order = kern.order;
    fn.degree = param.degree;
    fn.bw = param.bwvEst;
    fn.updateData(sivec);
    ApproxFn<double> efn = fn;
    vector<int> efnIndex(0);
    for(int j=efn.data.size()-1;j>=0;j--) {
      if (!(ltp1[j]>=0 && ltp1[j]<(int) sivec.size())) {
        efn.data.erase(efn.data.begin()+j);
        efn.mu.erase(efn.mu.begin()+j);
      } else {
        efnIndex.push_back(j);
      }
    }
    reverse(efnIndex.begin(),efnIndex.end());
    efn.updateData(efn.data);

    unsigned int N = exWeight.size();
    if (H!=NULL) delete[] H;
    if (pivec!=NULL) delete[] pivec;
    H = new double[N*N]; // I - discount * exWeight, so Vhat.val = H^{-1}*prof
    pivec = new double[N];
    if (param.degree==-1) { // specialized code for degree = 0, the
                            // general code in the else produces the
                            // same results (up to difference between
                            // Vhat.mu and eVhat.mu), so this case is
                            // not used anymore.
      for(unsigned int i=0;i<exWeight.size();i++) {
        double sumwex = 0;
        exWeight[i] = vector<double>(sivec.size(),0);   
        for(unsigned int j=0;j<exWeight.size();j++) {
          if (ltp1[j]>=0 && ltp1[j]<(int) exWeight.size()) {
            // weight for v[ltp1[j]]|s[i]
            double kij = kern(sivec[i],sivec[j],param.bwvEst*fn.mu[j],fn.invRootV);
            sumwex += (exWeight[i][j] = kij);
          }
        }
        for(unsigned int j=0;j<exWeight.size();j++) {
          exWeight[i][j]/=sumwex;
        }
        // FOR TESTING ONLY
        /*
        double *xwx=NULL, *xw=NULL;
        unsigned int nx,n;
        efn.getxwx(&xwx,&xw,nx,n, sivec[i], 1);
        if (n!=efn.data.size() || nx!=1) printf("CRAP");
        long info = dgels(nx,nx,n,xwx,nx,xw,nx);
        if (i<20) for (unsigned int j=0;j<20;j++) {
          printf("%4d %4d: exW=%.3g xw=%.3g\n",
                 i,efnIndex[j], exWeight[i][efnIndex[j]], xw[0+j*nx]);
        }
        delete[] xwx;
        delete[] xw;
        */
        //fprintf(out,"\n");
      }
    } else { // local polynomial
      for(unsigned int i=0;i<exWeight.size();i++) {
        exWeight[i] = vector<double>(sivec.size(),0); 
      }
#pragma omp parallel default(shared)
      {
#pragma omp for  
        for(unsigned int i=0;i<sivec.size();i++) {
          double *xwx=NULL, *xw=NULL;
          unsigned int nx,n;
          efn.getxwx(&xwx,&xw,nx,n, sivec[i], 1);
          int info;
          dgels('N',nx,nx,n,xwx,nx,xw,nx,&info);
          if (info) {
            printf("WARNING (computeExWeight) dgels info=%d\n"
                   "A is not full rank. Using dgelsy instead.\n",info);
            double rcond = 1e-12;
            int *jpvt = new int[nx];
            int rank;
            dgelsy(nx,nx,n,xwx,nx,xw,nx,jpvt,rcond,&rank,&info);
            delete[] jpvt;
            if (info) {
              printf("ERROR (computeExWeight) dgelsy info=%d\n",info);
              exit(info);
            }
          } 
          for (unsigned int j=0;j<efn.data.size();j++) {
            exWeight[i][efnIndex[j]] = xw[0 + j*nx];
          }
          delete[] xwx;
          delete[] xw;
        } // for(i)
      } // end parallel region
    } // fi
    int nzero = 0;
    for (unsigned int i=0;i<N;i++) {
      for(unsigned int j=0;j<N;j++) {
        H[i+j*N] = i==j? 1.0: 0.0; 
        if (invl[j]>=0 && invl[j]<(int) N) H[i+j*N] -= param.discount*exWeight[i][invl[j]]; 
        if (fabs(H[i+j*N])<1e-10) nzero++;
      }
    }    
    printf("%d of %d (%.1f%%)entries in H are zero\n",nzero,N*N,100.0*nzero/(1.0*N*N));
  }

  /**
     Update Vhat to match current param, invest, and eta
  */
  vector<int> eVhatIndex; 
  
  void updateVhat() {
    ProfitFn<double> profit(param);
    unsigned int N;
    double *Hwork;
    Vhat.bw = param.bwvEst;
    Vhat.kern.order = param.order;
    Vhat.degree = param.degree;
    Vhat.kern.type = epan;
    // set up matrix and vector
    if (initialized==0) {
      //this->estimateEtaAndPolicy();
      Vhat.updateData(invest.data); 
      if (Vhat.val.size()==0) {
        Vhat.val = vector<double>(Vhat.data.size());
      }
      this->computeExWeight();
      initialized=1;
      nI = 1;
      nV = 0;
      nE = 0;
    } else {
      // (nV<nI) { Vhat.updateData(invest.data); nV = nI; }
      Vhat.updateData(invest.data); 
      if (Vhat.val.size()==0) {
        Vhat.val = vector<double>(Vhat.data.size());
      }
    }
    N = exWeight.size();
    for(unsigned int i=0;i<N;i++) {
      State<double> state = getFirmState(data->market[0].state[0],0);
      state.unvec(Vhat.data[i]);
      pivec[i] = profit(state,invest.val[i]);
    }
    Hwork = new double[N*N];
    memcpy(Hwork,H,N*N*sizeof(double));
    Timer timer;
    int info=solveLS(N,N, 1, Hwork, N,pivec,N);
    printf("solveLS took %.3g seconds\n",timer.elapsed());
    for(unsigned int i=0;i<N;i++) {
      Vhat.val[i] = pivec[i];
      // set to simulated value
      /*
      { 
        unsigned int m,t,f;
        getmtf(i,m,t,f);
        State<double> state = getFirmState(data->market[m].state[t],f);
        state.eta = eta[m][t][f]; 
        Vhat.val[i] = profit(state,invest.val[i]) + 
          param.discount*data->game->evalue(state,data->market[m].state[t].invest[f],0);
      }
      */
    }

    /*
    { // set to sum of E[pi] discounted
      for(unsigned int i=0;i<N;i++) {
        unsigned int m,t,f;
        getmtf(i,m,t,f);
        State<double> state = getFirmState(data->market[m].state[t],f);
        state.eta = eta[m][t][f]; 
        Vhat.val[i] = profit(state,invest.val[i]);
      }
      for (int t=0;t<100;t++) {
        vector<double> vhatval = Vhat.val;
        for (unsigned int i=0;i<Vhat.val.size();i++) {
          double ev = 0;
          unsigned int m,t,f;
          getmtf(i,m,t,f);
          State<double> state = getFirmState(data->market[m].state[t],f);
          state.eta = eta[m][t][f];           
          for(unsigned int j=0;j<exWeight.size();j++) {            
            if (ltp1[j]>=0) {
              ev += exWeight[i][j]*vhatval[ltp1[j]];
            }
          }
          Vhat.val[i] = profit(state,invest.val[i]) + param.discount*ev;
        }
      }
      for(unsigned int i=0;i<N;i++) {
        unsigned int m,t,f;
        getmtf(i,m,t,f);
        State<double> state = getFirmState(data->market[m].state[t],f);
        state.eta = eta[m][t][f]; 
        printf("%4d: pivec=%6.3g vt=%6.3g sumEp=%6.3g\n",i,pivec[i],
               profit(state,invest.val[i]) + 
               param.discount*data->game->evalue(state,data->market[m].state[t].invest[f],0),
               Vhat.val[i]);
      }
    }
    */
      
#ifndef NDEBUG     
    // for checking whether solving for value function correctly. uses
    // htest.m to check linear algebra and consistency with
    // Vhat(Vhat.data[i])
    FILE *out = fopen("value.csv","w");
    for(unsigned int i=0;i<N;i++) {
      fprintf(out, "%.16g", pivec[i]);
      for(unsigned int j=0;j<N;j++) fprintf(out, ",%.16g", H[i+j*N]); 
      fprintf(out, "\n");
    }
    for(unsigned int i=0;i<N;i++) {
      fprintf(out,"%.16g,",pivec[i]);
    } 
    fprintf(out,"\n");
    for(unsigned int i=0;i<N;i++) {
      //Vhat.evalVerbose(Vhat.data[i]);
      fprintf(out,"%.16g,",Vhat(Vhat.data[i]));
    } 
    fprintf(out,"\n");
    fclose(out);  
    cout << "enter int to continue " << endl;
    cin >> info;
#endif

    delete[] Hwork;
    if (info) printf("WARNING updateVhat info=%d\n",info);
    // form E[vhat]
    eVhat.bw = Vhat.bw;
    eVhat.degree = Vhat.degree;
    eVhat.kern.order = Vhat.kern.order;
    eVhat.kern.type = Vhat.kern.type;
    eVhat.val.resize(Vhat.val.size());
    //if (nE<nI) { eVhat.updateData(sivec); nE = nI; }
    eVhat.updateData(sivec); 
    for(unsigned int j=0;j<Vhat.val.size();j++) {
      if (ltp1[j]>=0 && ltp1[j]<(int) Vhat.val.size()) 
        eVhat.val[j] = Vhat.val[ltp1[j]];
    }
    // delete elements of eVhat where no data 
    eVhatIndex = vector<int>(0);
    for(int j=Vhat.val.size()-1;j>=0;j--) {
      if (!(ltp1[j]>=0 && ltp1[j]<(int) Vhat.val.size())) {
        eVhat.val.erase(eVhat.val.begin()+j);
        eVhat.data.erase(eVhat.data.begin()+j);
        eVhat.mu.erase(eVhat.mu.begin()+j);
      } else {
        eVhatIndex.push_back(ltp1[j]);
      }
    }    
    reverse(eVhatIndex.begin(),eVhatIndex.end());
    eVhat.updateData(eVhat.data);
  }
    
  /** Computes the best response to future value function Vhat i.e.
      argmax_i profit(i,s) + E[vhat|i,s]
  */
  double bestAction(unsigned int i, double &vstar) {
    const double da = 0.2; // only search within this many of observed action
    double alo, ahi, a, am1, am2;
    double plo, phi, p;
    const double atol = 1e-8;
    const double gr = 0.38197;
    int it=0, itint=0, itsec=0;
    unsigned int m,t,f;
    getmtf(i,m,t,f);
    a = data->market[m].state[t].invest[f];
    alo = a - da;
    ahi = a + da;
    am1 = a - 1;
    am2 = a - 2;
    plo = vTilde(alo,i);
    phi = vTilde(ahi,i);
    p = vTilde(a,i);
    // use brent's method to find maximum
    while ((ahi - alo)>atol) {
      int needsection=0;
      double atry, ptry;
      // parabolic interpolation
      double den = (plo-p)*(ahi-a)+(phi-p)*(a-alo);
      if (den!=0 && (a !=
                     (atry = a + 0.5*( (plo-p)*(ahi-a)*(ahi-a)+(phi-p)*(a-alo)*(a-alo) ) / den) )
          && atry>alo && atry<ahi && fabs(am1-am2)>1.6*atol ) {
        //fabs(atry-a)<(1-gr)*fabs(am1-am2)) {
        ptry = vTilde(atry,i);
        itint++;  
        //cout << "quad interp " << atry << " " << ptry << endl;
        if (ptry>p) {
          am2 = am1; am1 = a;
          if (atry<a) {
            //cout << " atry<a ";
            ahi = a; phi = p;
          } else {
            //cout << " atry>=a ";
            alo = a; plo =p;
          }
          a = atry; p = ptry;
        } else { // parabola did not improve anything
          needsection = 1;
        }
      } else needsection=1;
      if (itint>5 && itint>itsec) needsection=1;
      // section search
      if (needsection) {
        if ((ahi-a) > (a-alo) ) {
          atry = a + (ahi-a)*gr;
          ptry = vTilde(atry,i);
          am2 = am1; am1 = a;
          if (ptry>p) {
            alo=a; plo=p;
            a=atry; p=ptry;
          } else {
            ahi = atry; phi=ptry;
          }
        } else {
          atry = a + (alo-a)*gr;
          ptry = vTilde(atry,i);
          am2 = am1; am1 = a;
          if (ptry>p) {
            ahi=a; phi=p;
            a=atry; p=ptry;
          } else {
            alo = atry; plo=ptry;
          }
        }
        itsec++;
      }
      it++;
      //cout << it << ": " << ahi-alo << " itint=" << itint << " itsec=" << itsec<< endl;
      if (it>5000) {
        cout << "Too many iterations " << ahi-alo << " itint=" << itint << " itsec=" << itsec << endl;
        break;
      }
    }
    vstar = vTilde(a,i);
    return(a);
  }

  double vTilde(double inv, unsigned int i) {
    ProfitFn<double> profit(param);
    unsigned int m,t,f;
    State<double> state = getFirmState(data->market[0].state[0],0);
    vector<double> si;
    double ev;
    getmtf(i,m,t,f);
    state.unvec(Vhat.data[i]);
    state.eta = eta[m][t][f];
    si = state.vec();
    si[si.size()-1] = inv;
    if (profit(state,inv)>20) printf("warning: prof=%6.3g\n",profit(state,inv));
    ev = eVhat(si);
    if (ev>20) { 
//       static int firstTime=1;
//       FILE *out;
//       if (firstTime) {
//         char filename[30];
//         sprintf(filename,"badsi%d.csv",omp_get_thread_num());
//         firstTime=0;
//         out = fopen(filename,"w");
//         printf("opened %s\n",filename);
//       }
//       printf("printing si\n");
//       for(unsigned int i=0;i<si.size();i++) fprintf(out,"%.8g, ",si[i]);
//       fprintf(out,"\n");
//       printf("printed si\n");
      if (i<10) printf("warning: ev = %6.3g\n",ev);
      //eVhat.evalVerbose(si);
      //cout << "left evalVerbose" << endl;
    }
    return(profit(state, inv) + param.discount*ev);
  }

  void computeStateDens() {
    FILE *out = fopen("stateDens.csv","w");
    stateDens = vector<double>(eVhat.val.size());
    for(int i=0;i<(int) stateDens.size();i++) {
      stateDens[i] = 0;
      for(unsigned int k=0;k<eVhat.data.size();k++) {
        if ((int) k!=i) {
          double w = eVhat.kern(eVhat.data[k],sivec[i],eVhat.bw*eVhat.mu[k],eVhat.invRootV);
          stateDens[i] += w;
        }
      }
      stateDens[i] /= (eVhat.data.size()-1);
      fprintf(out,"%.8g, ",stateDens[i]);
      for(unsigned int k=0;k<sivec[i].size();k++) fprintf(out,"%.8g, ",sivec[i][k]);
      fprintf(out,"\n");     
    }
    fclose(out);
    /*
    for(double q = 0;q<=1;q+=0.01) {
      double qtile=quantile<double>(stateDens,q);
      printf("%3.2lf quantile of stateDens=%6.3g\n",q,qtile);
    }
    */
    minDens = quantile<double>(stateDens,param.minDensQ);
  }

  void minValueMSE() {
    double valueMse(int n, const double *x, double *grad, void *appFn);      
    double x[1], xhi[1], xlo[1], cv;
    double *g=NULL;
    void *etaMseArgs[2];
    x[0] = fabs(param.bwvEst); 
    xlo[0] = 0.1>x[0]-2? 0.1:x[0]-2;
    xhi[0] = x[0]+2; 
    cout << "value bandwidth search start" << endl;
    etaMseArgs[0] = this; 
    etaMseArgs[1] = this->data->game;
    double minmse = 1e50;
    double xmin = 0.0;
    for(x[0]=xlo[0];x[0]<xhi[0];x[0]+=0.25) {
      double mse = valueMse(1,x,g,etaMseArgs);
      if (minmse>mse) {
        minmse = mse;
        xmin = x[0];
      }
    }
    x[0] = xmin;
    cv = minmse;
    
    int returnCode = 0;// NLOPT_LN_COBYLA
      /*
      nlopt_minimize_constrained(NLOPT_GN_DIRECT_L, 1, valueMse, //    cvObjective, (void*) &Fi
                                 etaMseArgs,
                                 0, NULL, NULL,0,
                                 xlo, xhi, x, &cv,
                                 -HUGE_VAL, 1e-8, 1e-6, 1e-4, NULL, 1000, 0);
    */
    /*                     ftol_rel, ftol_abs, xtol_rel, xtol_abs=NULL */ 
    param.bwvEst = x[0]; 
    printf("code = %d, bw = %g, cv=%g\n",returnCode,param.bwvEst,cv);
    nloptReturnCodeMessage(returnCode);
  } /************************************************************/
  
  
  void computeMatrices() {
    // set up matrices above
    Timer timer;
    unsigned int N;

    if (initialized==0) {
      this->estimateEtaAndPolicy();
      this->estimateTransitionDensity();
      this->computeExWeight();
    }
    N = exWeight.size();
    if (K==NULL) K = new double[N*KSIZE];
    if (dK==NULL) dK = new double[N*KSIZE];
    if (E==NULL) E = new double[N*N];
    if (dE==NULL) dE = new double[N*N];
    if (dKpdEHK==NULL) dKpdEHK = new double[N*KSIZE];
    if (HK==NULL) HK = new double[N*KSIZE];
    if (EV==NULL) EV = new double[N*KSIZE];
    if (dEV==NULL) dEV = new double[N*KSIZE];

#pragma omp parallel default(shared) 
    {
      
#pragma omp for
      // Make sure this matches the definition of profitFn
      for (unsigned int i=0;i<N;i++) {
        unsigned int m,t,f;
        double invest, ownQ;
        this->getmtf((unsigned int) i,m,t,f);      
        State<double> state = getFirmState(data->market[m].state[t],f);
        invest = data->market[m].state[t].invest[f];
        ownQ = state.ownQuality;
        
        K[i+0*N] = data->market[m].state[t].revenue[f];
        K[i+1*N] = -invest;
        K[i+2*N] = invest*eta[m][t][f];
        K[i+3*N] = -invest*invest;
        K[i+4*N] = -((invest>0 &&  ownQ>0)? invest*ownQ:0.0);
        
        dK[i+0*N] = 0.0;
        dK[i+1*N] = -1.0;
        dK[i+2*N] = eta[m][t][f];
        dK[i+3*N] = -2.0*invest;
        dK[i+4*N] = -((invest>0 &&  ownQ>0)? ownQ:0.0);
        if (param.degree==0) { // kernel regression
          double sumwex = 0;
          double sumde = 0;
          int lmu = 0;
          for(unsigned int j=0;j<N;j++) {        
            if (invl[j]>=0 && invl[j]<(int) N) {
              sumde += (dE[i+j*N] = 
                        eVhat.kern.deriv(sivec[i],sivec[invl[j]], 
                                         eVhat.bw*eVhat.mu[lmu], 
                                         eVhat.invRootV, sivec[i].size()-1) ); 
              // make sure sivec[i].size()-1 remains location of
              // investment in sivec
              sumwex += (E[i+j*N] = 
                         eVhat.kern(sivec[i],sivec[invl[j]], 
                                    eVhat.bw*eVhat.mu[lmu], eVhat.invRootV) );
              // if (i==0 && eVhatIndex[lmu]!=j) printf("j=%d, lmu=%d, eVhatIndex[lmu]=%d\n",j,lmu,eVhatIndex[lmu]);
              lmu++;
            } else dE[i+j*N] = E[i+j*N] = 0;                
          }
          for(unsigned int j=0;j<N;j++) {
            if (invl[j]>=0 && invl[j]<(int) N) {
              E[i+j*N] /= sumwex;
              dE[i+j*N] = (dE[i+j*N] - E[i+j*N]*sumde)/(sumwex);
            }
          }
        } else { // local polynomials
          double *xwx=NULL, *xw=NULL;
          unsigned int nx, n;
          eVhat.getxwx(&xwx,&xw,nx,n,sivec[i],1);
          int info;
          dgels('N',nx,nx,n,xwx,nx,xw,nx,&info);
          if (info) {
            printf("WARNING (computeMatrices) dgels info=%d, i=%d\n"
                   "Using dgelsy instead.\n"
                   ,info,i);
            double rcond = 1e-12;
            int *jpvt = new int[nx];
            int rank;
            dgelsy(nx,nx,n,xwx,nx,xw,nx,jpvt,rcond,&rank,&info);
            delete[] jpvt;
            if (info) {
              printf("ERROR (computeMatrices) dgelsy info=%d\n",info);
              exit(info);
            }
          }
          for(unsigned int j=0;j<N;j++) E[i+j*N] = dE[i+j*N] = 0; 
          for (unsigned int j=0;j<n;j++) {
            E[i+eVhatIndex[j]*N] = xw[0 + j*nx];
            dE[i+eVhatIndex[j]*N] = -xw[sivec[i].size() + j*nx];
            //printf("i=%d j=%d ltp1[j]=%d, N=%d n=%d\n",i,j,eVhatIndex[j],N,n);
          } // j
          delete[] xwx;
          delete[] xw;
        } // fi
      } // i    
    } // parallel region    
    // set dKpdEHK = H^{-1}*K
    int ksize = KSIZE;
    double *Hwork = new double[N*N];
    memcpy(Hwork,H,N*N*sizeof(double));
    memcpy(HK,K,N*KSIZE*sizeof(double));
    int info=solveLS(N,N, ksize, Hwork, N,HK,N);
    if (info) printf("WARNING: computeMatrices() solveLS returned %d\n",info);
    delete[] Hwork;
#ifdef _MKL
    cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, 
                N,ksize,N,1.0, E, N, HK, N, 0.0,EV, N); 
    // set dEV = dE*H^{-1}*K
    cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, 
                N,ksize,N,1.0, dE, N, HK, N, 0.0,dEV, N);        
    // set dKpdEHK = discount*dE*H^{-1}*K + dK
    memcpy(dKpdEHK,dK,N*KSIZE*sizeof(double));
    cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, 
                N,ksize,N,param.discount, dE, N, HK, N,1.0,dKpdEHK,
          N);
#else 
    // set EV = E* H^{-1}* K
    dgemm('N','N', N,ksize,N,1.0, E, N, HK, N, 0.0,EV, N); 
    // set dEV = dE*H^{-1}*K
    dgemm('N','N', N,ksize,N,1.0, dE, N, HK, N, 0.0,dEV, N);        
    // set dKpdEHK = discount*dE*H^{-1}*K + dK
    memcpy(dKpdEHK,dK,N*KSIZE*sizeof(double));
    dgemm('N','N',N,ksize,N,param.discount, dE, N, HK, N,1.0,dKpdEHK,
          N);
#endif
    printf("computeMatrices() took %.1f seconds\n",timer.elapsed());
  } // computeMatrices () ///////////////////////////////////////////////////
  
  void checkMatrices() 
  {
    double theta[KSIZE];
    unsigned int N = sivec.size();
    ProfitFn<double> profit(param);
    theta[0] = 1.0;
    //param.iCost[1] = param.iCost[2] = param.iCost[3] = 0;
    for(unsigned int j=0;j<param.iCost.size();j++) 
      theta[j+1] = param.iCost[j];
    for(unsigned int i=0;i<25 && i<sivec.size();i++) {
      double ev = 0, v = 0, prof = 0, dprof = 0, dev=0;
      unsigned int m,t,f;
      getmtf(i,m,t,f);
      State<double> state = getFirmState(data->market[m].state[t],f);
      for(unsigned int j=0;j<KSIZE;j++) {
        ev += EV[i+j*N]*theta[j];
        dev += dEV[i+j*N]*theta[j];
        v += HK[i+j*N]*theta[j];
        prof += K[i+j*N]*theta[j];
        dprof += dK[i+j*N]*theta[j];
      }
      state.eta = eta[m][t][f]; 
      printf("%2d: ev = %8.5g evhat = %8.5g vhat.val = %8.5g v = %8.5g"
             " prof = %8.5g pf = %8.5g\n",i,ev,eVhat(sivec[i]),
             Vhat.val[i],v,prof,
             profit(state,data->market[m].state[t].invest[f])
             );
      double devh = eVhat(sivec[i]);
      double dev2 = eVhat.deriv(sivec[i],sivec[i].size()-1);
      double vt = data->game->evalue(state,data->market[m].state[t].invest[f],0);
      double dtev =
        (vt 
         - 
         data->game->evalue(state, data->market[m].state[t].invest[f]-1e-4,0))
        /1e-4;
      //double dp = 
      //  (profit(state,data->market[m].state[t].invest[f]) - 
      //   profit(state,data->market[m].state[t].invest[f]-1e-4))
      //  /1e-4;
      vector<double> foo = sivec[i];
      foo[foo.size()-1] -= 1e-4;
      devh -= eVhat(foo);
      devh /= 1e-4;
      printf("   dev = %8.5g devhat = %8.5g devhat2 = %8.5g dtev = %8.5g evt=%8.5g\n",
             //"|||| dprof = %8.5g dp = %8.5g\n",
             dev,devh,dev2,dtev,vt);
      //eVhat.evalVerbose(sivec[i]);
      //for (int j=0;j<sivec.size();j++) {
      //  printf(" %6.3g",E[i+j*N]);
      //}
      printf("\n---------------------------------------------------------\n");
    }
  }
    
  // destructor
  ~Estimation() {
    if (H) { delete[] H; H=NULL; }
    if (pivec) { delete[] pivec; pivec=NULL; }
    if (K) { delete[] K; K=NULL; }
    if (dK) { delete[] dK; dK=NULL; }
    if (E) { delete[] E; E = NULL; }
    if (dE) { delete[] dE; dE = NULL; }
    if (dKpdEHK) { delete[] dKpdEHK; dKpdEHK=NULL; }
    if (EV) { delete[] EV; EV = NULL; }
    if (dEV) { delete[] dEV; dEV = NULL; }
    if (HK) { delete[] HK; HK = NULL; }
  }

  friend double objFunc2(int n, const double *x, double *grad, void *vptr);
  friend double valueMse(int n, const double *x, double *grad, void *args);
}; // class Estimation //////////////////////////////////////////////////////


// objective function
double objFunc(int n, const double *x, double *grad, void *vptr) {
  Estimation *est = (Estimation *) vptr;
  double val = 0;
  double mav = 0;
  int imod;
  Timer timer;
  est->setParam(x);
  est->updateVhat();
  printf("updateVhat took %.3g seconds\n",timer.elapsed());
  timer.reset();
  if (est->param.maxMoments>0 && est->param.maxMoments<(int) est->Vhat.val.size()) {
    imod=est->Vhat.val.size()/est->param.maxMoments;
  } else imod=1;
#pragma omp parallel default(shared) 
  {
    int work = 0;
    //Timer tm;
    //printf("hello from thread %d\n",omp_get_thread_num());
#pragma omp for reduction(+ : val, mav)
    for(int i=0;i<(int) est->Vhat.val.size();i+=imod) {
      if (est->stateDens[i]>est->minDens) {
        unsigned int m,t,f;
        double vstar;
        double di;
        double mv;
        est->getmtf(i,m,t,f);
        if (est->param.iObj) {
          double istar = est->bestAction(i,vstar);        
          di = (istar - est->data->market[m].state[t].invest[f]);
        //printf("vstar = %6.3g , vTilde = %6.3g, i = %3d, inv = %6.3g, istar = %6.3g\n", 
        //       vstar, est->vTilde(est->data->market[m].state[t].invest[f],i), 
        //       i,est->data->market[m].state[t].invest[f], istar);
          mv = fabs(est->data->market[m].state[t].invest[f]);
          val += di*di;
          mav += mv;
          if (est->param.vObj) {
            double vobs = est->vTilde(est->data->market[m].state[t].invest[f],i);
            mv = vobs;   
            di = vstar - vobs;
            val += di*di;
            mav += mv;
          }
        }
        else if (est->param.vObj) {
          double vobs = est->vTilde(est->data->market[m].state[t].invest[f],i);
          est->bestAction(i,vstar);
          di = (vstar - vobs); 
          mv = vobs;          
          if ((di*di)>10) {
#pragma omp critical 
            printf("dv*dv = %6.3g: obs %d, psi = %6.3g %6.3g %6.3g, skipping\n",di*di,i,x[0],x[1],x[2]);
          } else {
            val += di*di;
            mav += mv;
          }
        }
        if (est->param.fObj) {
          const double dx = 1e-4;
          di = (est->vTilde(est->data->market[m].state[t].invest[f]+dx,i) 
                -est->vTilde(est->data->market[m].state[t].invest[f]-dx,i))/(2*dx);
          mv = fabs(est->data->market[m].state[t].invest[f]); //fabs(di);
          val += di*di;
          mav += mv;
        }
      }
      work++;
    } // end for(i)
    //printf("goodbye from thread %d, work = %d, sec=%.3g\n",omp_get_thread_num(),work,
    //       tm.elapsed());
  } // end parallel region
  if (est->eVhat.nBad>0) {
    printf("bad eVhat %d times\n",est->eVhat.nBad);
    return(-100); // will result in nlopt exit due to minf_max=-1
  }
  if (est->param.divAbs) val/= mav; //(est->Vhat.val.size()/imod);
  else val/=(est->Vhat.val.size()/imod);
  printf("Objfunc: parallel loop took %.3g seconds\n",timer.elapsed());
  cout << "objFunc= " << val << ", x = ";
  for(int i=0;i<n;i++) cout << x[i] << " " ;
  cout << "mav = " << mav << endl;
  return(val);
}

double objFunc2(int n, const double *x, double *grad, void *vptr) {
  Estimation *est = (Estimation *) vptr;
  double val = 0;
  double theta[KSIZE];
  unsigned int N = est->sivec.size();
  est->setParam(x);
  theta[0] = 1.0; 
  for(int j=0;j<n;j++) theta[j+1] = x[j];
#pragma omp parallel default(shared) 
  {
    //Timer tm;
    //printf("hello from thread %d\n",omp_get_thread_num());
#pragma omp for reduction(+ : val)
    for(unsigned int i=0;i<N;i++) {
      double foc = 0;
      for(unsigned int j=0;j<KSIZE;j++) {
        foc += est->dKpdEHK[i+j*N]*theta[j];
      }
      val += foc*foc;
    }
  }
  val/=N;
  return(val);
} // objFunc2() /////////////////////////////////////////////////////////////////////////


/********************************************************************************/
void plotObj(int n, double *xlo, double *xhi, int np, nlopt_func_old fn, void *vptr) 
{
  double *x = new double[n];
  FILE *out = fopen("plotObj.csv","w");
  for(int i=0;i<n-1;i++) x[i] = xlo[i];
  x[n-1] = 0.5*(xhi[n-1]+xlo[n-1]);
  while(x[n-2]<=xhi[n-2]) {
    int k=0;
    for(int i=0;i<n;i++) fprintf(out,"%.12g , ",x[i]);
    fprintf(out,"%.12g\n",fn(n,x,NULL,vptr));
    while (k<n-1) {
      x[k] += (xhi[k]-xlo[k])/np;
      if (k<n-2 && x[k]>xhi[k]) {
        x[k] = xlo[k];
        k = k+1;
      } else break;        
    }
  }
  delete [] x;
  fclose(out);
}
/********************************************************************************/

double etaMse(int n, const double *x, double *grad, void *args) {
  grad = NULL;
  Estimation *est = (Estimation *) ((void**) args)[0];
  //double mean = 0;
  double mse = 0;
  //double mae = 0;
  double bw = x[0];
  double bwi = x[1];
  ApproxFn<double> *Fi = (ApproxFn<double> *) ((void**) args)[1];
  vector<double> *ivec = (vector<double> *) ((void**) args)[2];
  int count = 0;
#pragma omp parallel default(shared) 
  {
    State<double> fs;
    vector<double> dat;
    ApproxFn<double> FiCopy = (*Fi);
    FiCopy.bw = bw;
    //printf("FiCopy.order=%d FiCopy.degree=%d FiCopy.bw=%.2f\n",
    //       FiCopy.kern.order,FiCopy.degree,FiCopy.bw);
#pragma omp for reduction(+ : count, mse)
    for(unsigned int m=0;m<est->data->market.size();m++) {
      for(unsigned int t=0;t<est->data->market[m].state.size();t++) {
        for(unsigned int f=0;f<est->eta[m][t].size();f++) {
          double etaHat, de;
          fs = getFirmState(est->data->market[m].state[t],f);
          for(unsigned int i=0;i<(*ivec).size();i++) 
            FiCopy.val[i] = FiCopy.kern.integrate(est->data->market[m].state[t].invest[f],(*ivec)[i],bwi);
          dat.clear();
          dat.push_back(fs.ownQuality);

          for(unsigned int i=0;i<fs.otherQuality.size();i++) dat.push_back(fs.otherQuality[i]);
          for(unsigned int j=0;j<fs.x.size();j++) dat.push_back(fs.x[j]);
          etaHat = invnormcdf(FiCopy(dat),0,1);
          de = etaHat - est->data->market[m].state[t].eta[f];
          //if (est->geti(m,t,f) < 20) {
          //   printf("etaMse %d: etaHat=%.2g eta=%.2g\n"
          //         ,est->geti(m,t,f),etaHat,est->data->market[m].state[t].eta[f]);
          //}
          count++;
          //mean += de; 
          mse += de*de; 
          //mae += fabs(de);
        }
      }
    }
  } // end parallel region
  mse /= count;
  //mae /= count;
  //mean /= count;
  return(mse);
}

/********************************************************************************/

double valueMse(int n, const double *x, double *grad, void *args) {
  Estimation *est = (Estimation *) ((void**) args)[0];
  DynamicGame *dg = (DynamicGame *) ((void**) args)[1];
  //double mean = 0;
  double mse = 0, msed=0;
  //double mae = 0;
  double bw = x[0];
  unsigned int N = est->sivec.size();
  double theta[KSIZE];
  theta[0] = 1.0;
  for(unsigned int j=0;j<est->param.iCost.size();j++) theta[j+1] = est->param.iCost[j];
  //static FILE *out = NULL;
  grad = NULL;
  est->param.bwvEst = bw;
  est->Vhat.bw = bw;
  est->Vhat.updateData(est->Vhat.data);
  est->computeExWeight();
  est->updateVhat();
  est->computeMatrices();
#pragma omp parallel default(shared) 
  {
#pragma omp for reduction(+ : mse, msed)
    for(unsigned int i=0;i<est->sivec.size();i++) {
      unsigned int m, t, f;
      est->getmtf(i,m,t,f);
      State<double> state = getFirmState(est->data->market[m].state[t],f);
      double ev=0 ,dev=0, evt=0, devt=0;
      for(unsigned int j=0;j<KSIZE;j++) {
        ev += est->EV[i+j*N]*theta[j];
        dev += est->dEV[i+j*N]*theta[j];
      }
      evt = dg->evalue(state,est->data->market[m].state[t].invest[f],
                       i%(dg->eInt.size()/dg->nint));
      devt = (evt - dg->evalue(state,est->data->market[m].state[t].invest[f]-1e-4,
                               i%(dg->eInt.size()/dg->nint)))/1e-4;
      if (i<20) {
#pragma omp critical 
        {
          printf("%2d: ev=%6.3g evt=%6.3g dev=%6.3g devt=%6.3g evhat=%6.3g\n",
                 i,ev,evt,dev,devt,
                 est->eVhat(est->sivec[i]));
        }
      }
      mse += (ev-evt)*(ev-evt); 
      msed += (dev-devt)*(dev-devt);
    }
  }
  mse /= N;
  msed /= N;
  printf("bw=%6.3g mse=%6.3g msed=%6.3g \n",bw,mse,msed);
  //static FILE *out = NULL;
  //if (out==NULL)  out = fopen("valueMSE.csv","w");
  //fprintf(out,"%.8g, %.8g, %.8g\n",bw,mse,msed);
  //fflush(out);
  return(msed);
}
/********************************************************************************/

#ifdef _DEBUG
#include <signal.h>
#include <fenv.h>
#endif

int main(int narg, char *parg[]) 
{
  /*
#ifdef _DEBUG
  feenableexcept(FE_DIVBYZERO | 
                 //FE_INEXACT |
                 //FE_INVALID |
                 FE_OVERFLOW);
                 //FE_UNDERFLOW);
#endif
  */
  Param<double> theta;
  {   // set parameters
    string pfilename;
    if (narg==1) {
      cout << "No parameter file name given, using \"param.txt\"" << endl;
      pfilename = "param.txt";
    } else {
      pfilename = parg[1];
      cout << "Using parameter file \"" << pfilename << "\"" << endl;
    }
    theta.valuesFromFile(pfilename);
  }
  //theta.order = 2;
  cout << "Set parameters\n";  
  DynamicGame investGame(theta);
  State<double> state = investGame.stateSolve[0];
  investGame.howard = 0;
  //  investGame.updateSolveGrid(100);
  if (theta.useSavedEq) {
    investGame.load("eq.csv");
    {
      int s = investGame.stateSolve[0].vec().size();
      vector<double> avg(s,0);
      vector<double> sd(s,0);
      for(unsigned int i=0;i<investGame.stateSolve.size();i++) {
        vector<double> v = investGame.stateSolve[i].vec();
        for(unsigned int k=0;k<avg.size();k++) {
          avg[k] += v[k]/investGame.stateSolve.size();
        }
      }
      for(unsigned int i=0;i<investGame.stateSolve.size();i++) {
        vector<double> v = investGame.stateSolve[i].vec();
        for(unsigned int k=0;k<avg.size();k++) {
          sd[k] += (v[k]-avg[k])*(v[k]-avg[k])/investGame.stateSolve.size();
        }
      }
      for(unsigned int k=0;k<sd.size();k++) sd[k] = sqrt(sd[k]);
      cout << "avg state = " << avg << endl;
      cout << "sd  state = " << sd << endl;
      double a=0,v=0,sa=0,sv=0;
      for(unsigned int i=0;i<investGame.stateSolve.size();i++) {
        a += investGame.strategy(investGame.stateSolve[i]);
        v += investGame.value(investGame.stateSolve[i]);
      }
      a/= investGame.stateSolve.size();
      v/= investGame.stateSolve.size();
      for(unsigned int i=0;i<investGame.stateSolve.size();i++) {
        double d = investGame.strategy(investGame.stateSolve[i]) - a;
        sa += d*d;
        d = investGame.value(investGame.stateSolve[i]) - v;
        sv += d*d;
      }
      sa/= investGame.stateSolve.size();
      sv/= investGame.stateSolve.size();
      sa = sqrt(sa); sv = sqrt(sv);
      cout << "avg policy,value = " << a << " " << v << endl;
      cout << "sd  policy,value = " << sa << " " << sv << endl; 
    }
    /*
    int nx = investGame.value.val.size()*2;
    double *x = new double[nx];
    double *xlo = new double[nx];
    double *xhi = new double[nx];
    double objval;
    for(unsigned int i=0;i<investGame.value.val.size();i++) {
      xlo[i] = 0;
      xhi[i] = 10;
      x[i] = investGame.value.val[i];
      x[i+investGame.value.val.size()] = investGame.strategy.val[i];
      xlo[i+nx/2] = 0;
      xhi[i+nx/2] = 10;
    }
    printf("eqfunc=%g\n",eqFunc(nx,x,NULL,(void*) &investGame));
    int returnCode = 
      nlopt_minimize_constrained(NLOPT_LN_SBPLX, nx, eqFunc, (void*) &investGame,
                                 0, NULL, NULL,0,
                                 xlo, xhi, x, &objval,
                                 -1, 1e-12, 1e-12, 1e-3, NULL, 1000,0);
    printf("nlopt returned %d, eqfunc=%g\n",returnCode,objval);
    nloptReturnCodeMessage(returnCode);
    for(unsigned int i=0;i<investGame.value.val.size();i++) {
      investGame.value.val[i]=x[i];
      investGame.strategy.val[i]=x[i+nx/2];
    } 
    */
    if (theta.nCheck>0) checkEq(investGame,theta.nCheck);
  }
  else { 
    for (int t=0;t<theta.eqRep;t++) {
      if (1 && t>0) {
        double cv;
        double xlo[1] = {1e-4}, xhi[1] = {10}, x[1];
        investGame.updateSolveGrid(t);
        if (0) { // update bandwidth
          int returnCode;
          cout << "bandwidth search start" << endl;
          /*
            x[0] = investGame.value.bw;
           returnCode = 
            nlopt_minimize_constrained(NLOPT_LN_COBYLA, 1, cvObjReg, (void*) &investGame.value,
                                     0, NULL, NULL,0,
                                     xlo, xhi, x, &cv,
                                       -HUGE_VAL, 1e-6, 1e-10, 1e-6, NULL, 1000, 0); 
          //                     ftol_rel, ftol_abs, xtol_rel, xtol_abs=NULL 
          
          investGame.value.bw = x[0]; 
          printf("value: code = %d, bw = %g, cv=%g\n",returnCode,x[0],cv);        
          x[0] = investGame.strategy.bw;
          returnCode = 
            nlopt_minimize_constrained(NLOPT_LN_COBYLA, 1, cvObjReg, (void*) &investGame.strategy,
                                       0, NULL, NULL,0,
                                       xlo, xhi, x, &cv,
                                       -HUGE_VAL, 1e-6, 1e-10, 1e-6, NULL, 1000, 0);
          //                     ftol_rel, ftol_abs, xtol_rel, xtol_abs=NULL 
          investGame.strategy.bw = x[0]; 
          printf("invest: code = %d, bw = %g, cv=%g\n",returnCode,x[0],cv);
          nloptReturnCodeMessage(returnCode);
          */
          cvStepAhead cvsa(&investGame);
          cvsa.valuecv=1;
          x[0] = investGame.value.bw;
          returnCode =             
            // NLOPT_LN_COBYLA,  NLOPT_GN_DIRECT_L
            nlopt_minimize_constrained(NLOPT_LN_COBYLA, 1, cvObjStep, (void*) &cvsa,
                                       0, NULL, NULL,0,
                                       xlo, xhi, x, &cv,
                                       -HUGE_VAL, 1e-4, 1e-6, 1e-6, NULL, 1000, 0);
          //                     ftol_rel, ftol_abs, xtol_rel, xtol_abs=NULL 
          investGame.value.bw = x[0]; 
          printf("value: code = %d, bw = %g, cv=%g\n",returnCode,x[0],cv);        
          /*
          x[0] = investGame.strategy.bw;
          cvsa.valuecv=0;
          returnCode = 
            nlopt_minimize_constrained(NLOPT_GN_DIRECT_L, 1, cvObjStep, (void*) &cvsa,
                                       0, NULL, NULL,0,
                                       xlo, xhi, x, &cv,
                                       -HUGE_VAL, 1e-4, 1e-6, 1e-6, NULL, 1000, 0);
          //                     ftol_rel, ftol_abs, xtol_rel, xtol_abs=NULL 
          investGame.strategy.bw = x[0]; 
          printf("invest: code = %d, bw = %g, cv=%g\n",returnCode,x[0],cv);
          nloptReturnCodeMessage(returnCode);
          */
        }
      } // if t>0
      investGame.computeEquilibrium();
      cout << "Computed equilibrium\n";
      {
        int s = investGame.stateSolve[0].vec().size();
        vector<double> avg(s,0);
        vector<double> sd(s,0);
        fstream fout; 
        for(unsigned int i=0;i<investGame.stateSolve.size();i++) {
          vector<double> v = investGame.stateSolve[i].vec();
          for(unsigned int k=0;k<avg.size();k++) {
            avg[k] += v[k]/investGame.stateSolve.size();
          }
        }
        for(unsigned int i=0;i<investGame.stateSolve.size();i++) {
          vector<double> v = investGame.stateSolve[i].vec();
          for(unsigned int k=0;k<avg.size();k++) {
            sd[k] += (v[k]-avg[k])*(v[k]-avg[k])/investGame.stateSolve.size();
          }
        }
        for(unsigned int k=0;k<sd.size();k++) sd[k] = sqrt(sd[k]);
        cout << "avg state = " << avg << endl;
        cout << "sd  state = " << sd << endl;
        double a=0,v=0,sa=0,sv=0;
        for(unsigned int i=0;i<investGame.stateSolve.size();i++) {
          a += investGame.strategy(investGame.stateSolve[i]);
          v += investGame.value(investGame.stateSolve[i]);
        }
        a/= investGame.stateSolve.size();
        v/= investGame.stateSolve.size();
        for(unsigned int i=0;i<investGame.stateSolve.size();i++) {
          double ai = investGame.strategy(investGame.stateSolve[i]);
          double vi = investGame.value(investGame.stateSolve[i]);
          sa += (ai-a)*(ai-a);
          sv += (vi-v)*(vi-v);
        }
        sa/= investGame.stateSolve.size();
        sv/= investGame.stateSolve.size();
        sa = sqrt(sa); sv = sqrt(sv);
        cout << "avg policy,value = " << a << " " << v << endl;
        cout << "sd  policy,value = " << sa << " " << sv << endl; 
      }
      checkEq(investGame,theta.nCheck);
      cout << "computed equilibrium " << t+1 << " times." << endl;
      printf("%d calls to value, %d nn used\n",investGame.value.calls, investGame.value.neighbor);
      printf("%d calls to strategy, %d nn strategy\n",investGame.strategy.calls, investGame.strategy.neighbor);
    }
    investGame.save("eq.csv");
  }
  if (!theta.estimate) exit(0);
  // simulate data
  //checkEq(investGame);
  char dateTime[100];
  char filename[110];
  Timer timer;
  time_t rawtime;
  time(&rawtime);
  struct tm * timeinfo = localtime(&rawtime);
  strftime(dateTime, 100, "%Y-%m-%d_%H%M", timeinfo);
  sprintf(filename,"est_%s.csv",dateTime);
  FILE *estFile = fopen(filename,"w");
  for(int b=0;b<theta.nRep;b++) {    
    Data sdata(&theta,&investGame);
    sdata.simulate();
    // set up estimation structure
    Estimation est;
    est.param = theta;
    est.data = &sdata;
    est.estimateEtaAndPolicy();
    est.estimateTransitionDensity(); //Jasmine Test

    vector<vector<vector< double > > > eta = est.eta;
    {
      FILE *out = fopen("eta.csv","w");
      double mean=0, mse=0, mae=0, mde = 0;
      int N= eta.size()*eta[0].size()*eta[0][0].size();
      int n = 0;
      for(unsigned int m=0;m<sdata.market.size();m++) {
        for(unsigned int t=0;t<sdata.market[m].state.size();t++) {
          for(unsigned int f=0;f<eta[m][t].size();f++) {
            double de = eta[m][t][f] - sdata.market[m].state[t].eta[f];
            if (n<20) cout << n << " etaHat=" << eta[m][t][f] << " eta=" << sdata.market[m].state[t].eta[f] << endl;
            //0.5*(1+erf(sdata.market[m].state[t].eta[f]/sqrt(2))) << endl;
            n++;
            mean += de; mse += de*de; mae += fabs(de);
            mde += fabs(eta[m][t][f]) - fabs(sdata.market[m].state[t].eta[f]);
            if (theta.useEta) {
              State<double> fs = getFirmState(sdata.market[m].state[t],f);
              fs.eta = est.eta[m][t][f] = sdata.market[m].state[t].eta[f];
              est.invest.data[est.geti(m,t,f)] = fs.vec();
            }
            fprintf(out,"%.8g, %.8g, %8g\n",sdata.market[m].state[t].invest[f],
                    sdata.market[m].state[t].eta[f],
                    est.eta[m][t][f] );
          }
        }
      }
      fclose(out);
      cout << "bwEst=" << est.param.bwEst << " bwiEst=" << est.param.bwiEst << endl;
      cout << "mean eta err = " << mean/N << endl
           << "mse  eta     = " << mse/N << endl
           << "m.abs.e eta  = " << mae/N << endl
           << "|hat|-|true| = " << mde/N << endl;
      
    }
    est.Vhat.updateData(est.invest.data); 
    est.Vhat.val = vector<double>(est.Vhat.data.size());
    est.computeExWeight();
    est.updateVhat();
    est.computeStateDens();
    est.initialized=1;
    est.computeMatrices();
    if (b==0) est.checkMatrices();

    if (theta.bwvEst<0) est.minValueMSE();    
    
    // initial values
    int nx = (int) theta.iCost.size(); // + theta.gamma.size()
    double *x = new double[nx];
    double *x0= new double[nx];
    double *xhi = new double[nx];
    double *xlo = new double[nx];
    int j;
    double objval;
    //for(unsigned int i=0;i<theta.gamma.size();i++) x[i]=x0[i] =
    //theta.gamma[i];
    j = 0;//(int) theta.gamma.size();
    for(unsigned int i = 0;i<theta.iCost.size();i++) {
      x0[i+j] = theta.iCost[i];
      x[i+j] = x0[i+j];
      x[i+j] = theta.psiStart[i];
      //x[i+j] = 0;
    }
    est.setParam(x0);
    if (b==0 && theta.plotObj>0) {
      for(int i=0;i<nx;i++) {
        xlo[i] = x[i]-0.5;
        xhi[i] = x[i]+0.5;
      }
      xlo[nx-1] = x[nx-1]; xhi[nx-1] = x[nx-1];
      plotObj(nx,xlo, xhi, theta.plotObj, objFunc2, (void*)&est); 
    }
    for(int i=0;i<nx;i++) {
      xlo[i] = x[i]-1.5;
      xhi[i] = x[i]+1.5;
      //x[i] *= 1.2;
    }
    xhi[nx-1] = -(xlo[nx-1] = -1e-4);
    //nx = 1;  
    //x[nx-1] = 0.5;
    //x0[nx-1] = theta.iCost;
    // estimate
    Timer estTime;
    
    int returnCode = 
      nlopt_minimize_constrained(NLOPT_LN_COBYLA, nx, objFunc2, (void*) &est,
                                 0, NULL, NULL,0,
                                 xlo, xhi, x, &objval,
                                 -1, theta.ftolRel, theta.ftolAbs, theta.xtolRel, NULL, 1000, 0);
    nloptReturnCodeMessage(returnCode);
    
    
    cout << "objval = " << objval << endl
         << "parameters: " << endl
         << "true = ";
    for(int i=0;i<nx;i++) cout << x0[i] << "  ";
    cout << endl << "est  = ";
    for(int i=0;i<nx;i++) cout << x[i] << "  ";
    cout << endl;
    cout << "estimation took " << estTime.elapsed() << " seconds" << endl;
    
    if (b==0 && theta.nRep==0) { // save estimated value function
      FILE *out=fopen("efunc.csv","w");
      est.setParam(x);
      est.updateVhat();
      for(int s=0;s<(int) est.Vhat.val.size();s++) {
        unsigned int m,t,f;
        est.getmtf(s,m,t,f);
        fprintf(out,"%.8g,%.8g,%.8g",est.invest(est.invest.data[s]),
                est.Vhat.val[s],
                est.vTilde(est.data->market[m].state[t].invest[f], s));
        for(int j=0;j<(int) est.Vhat.data[s].size();j++) {
          fprintf(out,",%.8g",est.Vhat.data[s][j]);
      }
        fprintf(out,"\n");
      }
      fclose(out);
      
      out=fopen("efunc0.csv","w");
      FILE *dout = fopen("data.csv","w");
      est.setParam(x0);
      est.updateVhat();
      for(int s=0;s<(int) est.Vhat.val.size();s++) {
        unsigned int m,t,f;
        est.getmtf(s,m,t,f);
        State<double> state = getFirmState(est.data->market[m].state[t],f);
        vector<double> si;
        double inv;
        state.unvec(est.Vhat.data[s]);
        state.eta = est.eta[m][t][f];
        si = state.vec();
        inv = si[si.size()-1] = est.data->market[m].state[t].invest[f];
        si[si.size()-1] = inv;
        fprintf(out,"%.8g,%.8g,%.8g",est.invest(est.invest.data[s]), 
                est.eVhat(si),
                est.eVhat.deriv(si,si.size()-1));
        for(int j=0;j<(int) est.Vhat.data[s].size();j++) {
          fprintf(out,",%.8g",est.Vhat.data[s][j]);          
        }
        ProfitFn<double> profit(est.param);
        double ev;
        double tval;
        double dval;
        int integralIndex = s%(investGame.eInt.size()/investGame.nint);
        ev = est.eVhat(si);
        tval = investGame.evalue(state,inv, integralIndex);
        dval = (tval-investGame.evalue(state,inv - 1e-4, integralIndex))/1e-4;
        fprintf(out,", %.8g , %.8g", profit(state,inv), ev);                
        fprintf(out,", %.8g, %.8g\n",tval, dval);
        fprintf(dout,"%d,%d, %.8g, %.8g, %.8g \n",
                m,t,profit(state,inv), 
                est.vTilde(est.data->market[m].state[t].invest[f], s),
                investGame.value(est.Vhat.data[s]));
      }
      fclose(out);
      fclose(dout);
      
      out = fopen("allsi.csv","w");
      int imod;
      if (est.param.maxMoments>0 && est.param.maxMoments<(int) est.Vhat.val.size()) {
        imod=est.Vhat.val.size()/est.param.maxMoments;
      } else imod=1;
      for(int i=0;i<(int) est.Vhat.val.size();i+=imod) {
        unsigned int m,t,f;
        est.getmtf(i,m,t,f);
        State<double> state = getFirmState(sdata.market[0].state[0],0);
        vector<double> si;
        est.getmtf(i,m,t,f);
        state.unvec(est.Vhat.data[i]);
        state.eta = est.eta[m][t][f];
        si = state.vec();
        si[si.size()-1] = sdata.market[m].state[t].invest[f];
        for(unsigned int i=0;i<si.size();i++) fprintf(out,"%.8g, ",si[i]);
        fprintf(out,"\n");
      }
      fclose(out);

      // print results
      fprintf(estFile,"rep, iCost[0],iCost[1],iCost[2], bw, bwi\n" 
             "true, %.8g , %.8g, %.8g \n", x0[0], x0[1], x0[2]);
    }
    fprintf(estFile,"%d, ", b);
    for(int i=0;i<nx ;i++) fprintf(estFile," %.8g ,",x[i]);
    fprintf(estFile,"%.8g, %.8g, %.8g \n", est.param.bwEst, est.param.bwiEst,est.param.bwvEst);
    fflush(estFile);
    int minutes =timer.elapsed()/60; 
    printf("completed %d of %d repetitions in %d:%.2f\n", 
           b, theta.nRep,minutes,timer.elapsed()-minutes*60);
    printf("###########################################################################\n");
    delete[] x;
    delete[] xlo;
    delete[] xhi;
    delete[] x0;
  }
  fclose(estFile);
  
  return(0);
}


