#ifndef LINALG_H
#define LINALG_H
#define solveLS dgelsMagma
#include<vector>
using std::vector;
//#define solveLS dgelss
//#define solveLS dgesvMagma
// interface to lapack

// least squares using qr
long dgelsMagma(int m, int n, int nrhs, double* a, int lda,
                double* b, int ldb);
long dgesvMagma(int m, int n, int nrhs, double* a, int lda,
                double* b, int ldb);

/// invert a matrix
void invert(double *A, int n);

/// take (A)^{-1/2} for sym psd matrix a and return det(A)
double invRoot(double *A, int n) ;

/// returns A^{-1/2}*det(A)^{1/(2n)}, so the return value as det=1
vector<vector<double> > invRootDdet(const vector<vector<double> > &A);

/// inverse normal cdf
double invnormcdf(double p,const double mu,const double var);
double rhoL2(const vector<double> &x,const vector<double> &y);

double rhol2sq(const vector<double> &x,const vector<double> &y);
double rhol2(const vector<double> &x,const vector<double> &y);
void nloptReturnCodeMessage(int returnCode);
/*
**  IPOW.C - Raise a number to an integer power
**
**  public domain by Mark Stephen with suggestions by Keiichi Nakasato
*/
double ipow(double x, int n);
#endif
