clear;
dat = csvread('H.csv');
n = size(dat,1)/2;
K = dat(1:n,1:n);
H = dat((n+1):end,1:n);
bW = -H + K;
dat = csvread('value.csv');
prof = dat(1:(end-2),1);
H2 = dat(1:(end-2),2:(end));
v = dat(end-1,1:(end-1))';
v2 = dat(end,1:(end-1))';
vm = H2 \ prof;
fprintf('||prof-H*v|| = %.4g\n',norm(prof-H*v));
fprintf('||v2-K*v|| = %.4g\n',norm(v2-K*v));
