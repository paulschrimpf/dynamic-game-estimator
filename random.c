/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////// RANDOM NUMBER GENERATION ///////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*  Parameters for: 
    Marsaglia & Tsang generator for random normals & random exponentials.
    Marsaglia, G. & Tsang, W.W. (2000) `The ziggurat method for generating random variables', J. Statist. Software, 
    v5(8). This is an electronic journal which can be downloaded from:  http://www.jstatsoft.org/v05/i08 */
#include<math.h>
#include<stdio.h>
#include<time.h>

#include "random.h"


static inline unsigned int SHR3(struct randState *rs) 
{ return(rs->jz=rs->jsr, 
         rs->jsr^=(rs->jsr<<13), 
         rs->jsr^=(rs->jsr>>17), 
         rs->jsr^=(rs->jsr<<5),
         rs->jz+rs->jsr); }
static inline double UNI(struct randState *rs) 
{ return(.5 + (signed) SHR3(rs)*.2328306e-9); }
#define IUNI(rs) SHR3(rs)
#define EPSILON  1.110326e-16

static double nfix(struct randState *rs)
{ // nfix() generates variates from the residue when rejection in RNOR occurs.
  const double r = 3.442620f;     /* The start of the right tail */
  static double x, y;
  for(;;) {
    x=rs->hz*rs->wn[rs->iz];      /* iz==0, handles the base strip */
    if(rs->iz==0) {
      do {
	x=-log(UNI(rs))*0.2904764; /* .2904764 is 1/r */
        y=-log(UNI(rs));
      } while(y+y<x*x);
      return (rs->hz>0)? r+x : -r-x;
      }
    /* iz>0, handle the wedges of other strips */
    if ( rs->fn[rs->iz]+UNI(rs)*(rs->fn[rs->iz-1]-rs->fn[rs->iz]) < exp(-.5*x*x) ) return x;
    /* initiate, try to exit for(;;) for loop*/
    rs->hz=SHR3(rs);
    rs->iz=rs->hz&127;
    if(fabs(rs->hz)<rs->kn[rs->iz]) return (rs->hz*rs->wn[rs->iz]);
  }
}

static double efix(struct randState *rs)
{ // efix() generates variates from the residue when rejection in REXP occurs. 
  double x;
  for(;;) {
    if(rs->iz==0) return (7.69711-log(UNI(rs)));          /* iz==0 */
    x=rs->jz*rs->we[rs->iz];
    if( rs->fe[rs->iz]+UNI(rs)*(rs->fe[rs->iz-1]-rs->fe[rs->iz]) < exp(-x) ) return (x);
    /* initiate, try to exit for(;;) loop */
    rs->jz=SHR3(rs);
    rs->iz=(rs->jz&255);
    if(rs->jz<rs->ke[rs->iz]) return (rs->jz*rs->we[rs->iz]);
  }
}

static inline double RNOR(struct randState *rs) 
{ return(rs->hz=SHR3(rs), rs->iz=rs->hz&127, 
         (fabs(rs->hz)<rs->kn[rs->iz])? rs->hz*rs->wn[rs->iz] : nfix(rs)); }
static inline double REXP(struct randState *rs) 
{ return(rs->jz=SHR3(rs), rs->iz=rs->jz&255, 
         (rs->jz <rs->ke[rs->iz])? rs->jz*rs->we[rs->iz] : efix(rs)); }

unsigned int get_jsr(struct randState *rs)
{
  return(rs->jsr);
}

void set_jsr(unsigned int jsrin, struct randState *rs)
{
  rs->jsr = jsrin;
  return;
}

// This helps when you want to start the seuqence at the same place again
void reset_seed(unsigned int jsrseed, struct randState *rs)
{ // This procedure sets the seed and creates the tables.  
  //************************** THIS IS WHAT MAKES IT DIFFERENT FROM SET_SEED ****************************************//
  rs->jsr = 123456789; 
  //*****************************************************************************************************************//
  rs->jsr^=jsrseed;
}

void set_seed(unsigned int jsrseed, struct randState *rs)
{ // This procedure sets the seed and creates the tables.  
  const double m1 = 2147483648.0, m2 = 4294967296.;
  double dn=3.442619855899,tn=dn,vn=9.91256303526217e-3, q;
  double de=7.697117470131487, te=de, ve=3.949659822581572e-3;
  int i;
  rs->jsr = 123456789;
  rs->jsr^=jsrseed;
  /* Set up tables for RNOR */
  q=vn/exp(-.5*dn*dn);
  rs->kn[0]=(int)((dn/q)*m1);
  rs->kn[1]=0;
  rs->wn[0]=q/m1;
  rs->wn[127]=dn/m1;
  rs->fn[0]=1.;
  rs->fn[127]=exp(-.5*dn*dn);
  for(i=126;i>=1;i--) {
    dn=sqrt(-2.*log(vn/dn+exp(-.5*dn*dn)));
    rs->kn[i+1]=(int)((dn/tn)*m1);
    tn=dn;
    rs->fn[i]=exp(-.5*dn*dn);
    rs->wn[i]=dn/m1;
  }
  /* Set up tables for REXP */
  q = ve/exp(-de);
  rs->ke[0]=(int)((de/q)*m2);
  rs->ke[1]=0;
  rs->we[0]=q/m2;
  rs->we[255]=de/m2;
  rs->fe[0]=1.;
  rs->fe[255]=exp(-de);
  for(i=254;i>=1;i--) {
    de=-log(ve/de+exp(-de));
    rs->ke[i+1]= (int)((de/te)*m2);
    te=de;
    rs->fe[i]=exp(-de);
    rs->we[i]=de/m2;
  }
  rs->seed_initialized=1;
}

void set_time_seed(RANDSTATE *rs)
{
  time_t t;
  set_seed((unsigned int) time(&t),rs);
}

double Sample_Uniform(const double a,const double b, struct randState *rs)
{ 
  time_t t;
  if (rs->seed_initialized!=1) set_seed((unsigned int) time(&t),rs);
  return(UNI(rs)*(b-a) + a);
}

double Sample_Normal(const double mu,const double var, struct randState *rs)
{
  time_t t;
  if (rs->seed_initialized!=1) set_seed((unsigned int) time(&t),rs);
  return(RNOR(rs)*sqrt(var) + mu);
}


#define T4 0.45
double Sample_Truncated_Normal(const double mu,const double var,const double a,const int lb, struct randState *rs) 
{ // Returns one draw from a truncated normal with underlying mean=mu and VARIANCE=var with truncation
  //           (a,+infty)    IF lb=TRUE(not 0)
  //           (-infty,a)    IF lb=FALSE(0)
  double u,z,phi_z,c;
  time_t t;
  if (rs->seed_initialized!=1) set_seed((unsigned int) time(&t),rs);
  c=(a-mu)/(sqrt(var));
  if (!lb) c=-c;
  if (c < T4) {
    // normal rejection sampling
    do {
      u=RNOR(rs);
    } while(u<=c);
  }
  else {
    // exponential rejection sampling
    do {
      u = UNI(rs);
      z = REXP(rs)/c;
      phi_z=exp(-.5*(z*z));
    } while(u>=phi_z);
    u=c + z;
  }
  return !lb ? (mu-(u*sqrt(var))): (mu+(u*sqrt(var)));
}
#undef T4

#define T1 0.375
#define T2 2.18
#define T3 0.725
#define T4 0.45
#define F(x) exp(-.5*(x*x))
double Sample_Double_Truncated_Normal(const double mu,const double var,const double a,const double b, struct randState *rs)
{ // Generates one draw from truncated standard normal distribution (mu,sigma) on (a,b)
  double c,c1,c2,u[2],x,cdel,f1,f2,z,az,bz;
  int lflip,j;
  time_t t;
  if (rs->seed_initialized!=1) set_seed((unsigned int) time(&t),rs);
  c1=az=(a-mu)/sqrt(var);
  c2=bz=(b-mu)/sqrt(var);
  lflip=0;
  if (c1*c2<0.0) {
    if ((F(c1)>T1) && (F(c2)>T1)) {
      cdel=c2-c1;
      do {
	for(j=0;j<=1;j++) u[j] = UNI(rs);
	x=c1+cdel*u[0];
      } while (!(u[1]<F(x)));
    }
    else
      do {
	x=RNOR(rs);
      } while (!((x>c1) && (x<c2)));
  }
  else {
    if (c1<0.0) {
      c=c1;
      c1=-c2;
      c2 = -c;
      lflip=1;
    }
    f1=F(c1);
    f2=F(c2);
    if ( (f2<EPSILON) || (f1/f2>T2)) {
      if (c1>T3){
	// exponential rejection sampling
	c=c2-c1;
	do {
	  u[0] = UNI(rs);
	  z = REXP(rs)/c1;
	} while (!((z<c) && (u[0]<F(z))));
	x=c1+z;
      }
      else {
	// half-normal rejection sampling
	do {
	  x=RNOR(rs);
	  x=fabs(x);
	} while (!((x>c1) && (x<c2)));
      }
    }
    else {
      // uniform rejection sampling
      cdel=c2-c1;
      do {
	for(j = 0;j<= 1;j++) u[j] = UNI(rs);
	x=c1+cdel*u[0];
      } while (!(u[1] < F(x)/f1));
    }
  }
  return lflip ? (mu-sqrt(var)*x):(mu+sqrt(var)*x);
}
#undef F
#undef T1
#undef T2
#undef T3
#undef T4

double Sample_Gamma(const double ain,const double b, struct randState *rs)
{ // Returns a number distributed as a gamma distribution with shape parameter a and inverse scale b.
  // such that mean=a/b and var=a/(b*b). That is P(x) = [(b^a)/Gamma(a)] * [x^(a-1)] * exp(-x*b) for x>0 and a>=1.
  // Uses the algorithm in Marsaglia, G. and Tsang, W.W. (2000) `A simple method for generating
  // gamma variables', Trans. om Math. Software (TOMS), vol.26(3), pp.363-372.
  double d,c,x,v,u,a=ain,correction=1.0;
  time_t t;
  if (rs->seed_initialized==0) set_seed((unsigned int) time(&t),rs);
  if (a<1.0) {
    correction=pow(UNI(rs),1.0/a);
    a+=1.0;
  }
  d = a-1.0/3.0;
  c = 1.0/sqrt(9.0*d);
  for(;;) {
    do {
      x=RNOR(rs);
      v=1.0+c*x;
    } while(v<=0.0);
    v=v*v*v;
    u=UNI(rs);
    if( u<1.0-.0331*(x*x)*(x*x) ) return (correction*d*v/b);
    if( log(u)<0.5*x*x + d*(1.0-v+log(v)) ) return (correction*d*v/b);
  }
}

#define MAXITER 1000
double Sample_Truncated_Gamma(const double ain, const double b, 
                              const double lo, const double hi, struct randState *rs) 
{ // Returns a number distributed as a truncated gamma distribution with shape parameter a and inverse scale b.
  // such that mean=a/b and var=a/(b*b). That is P(x) = [(b^a)/Gamma(a)] * [x^(a-1)] * exp(-x*b) for x>0 and a>=1.
  // The distribution is truncated to [lo,hi].  Set lo<=0 and hi=INFINITY or NaN for no left/right truncation
  //
  double x;
  int iter=0;
  if (lo<=0 && !finite(hi)) return(Sample_Gamma(ain,b,rs));
  else if (lo<=0) { // only right truncation
    // simple rejection sampling
    x = hi+1;
    while (x>hi && ++iter<MAXITER) x = Sample_Gamma(ain,b,rs);
    // NOTE: this may be very efficient, but it isn't needed in the current code anyway ...
  } else if (!finite(hi)) { // only left truncation
    // simple rejection sampling
    x = lo-1;
    while (x<lo && ++iter<MAXITER) x = Sample_Gamma(ain,b,rs);
    // NOTE: this may be very efficient, but it isn't needed in the current code anyway ...
  } else { // truncated on both sides
    // simple rejection sampling
    x = hi+1;
    while ((x>hi || x<lo) && ++iter<MAXITER) x = Sample_Gamma(ain,b,rs);
    // NOTE: this may be very efficient, but it isn't needed in the current code anyway ...    
  }
  if (iter>=MAXITER) {
    printf("Sample_Truncated_Gamma: max iter reached\n");
    return(lo*1.01);
  } else return(x);
}
#undef MAXITER

double gamcdf(const double x,const double a,const double b)
{ // Gives me the gamma cdf such that mean=a/b and var=a/(b*b)
  // That is F(x) = integral(0,x) of [(b^a)/Gamma(a)] * [x^(a-1)] * exp(-x*b) for x>0 and a>=1.
  double p;
  double _gammp(double, double);
  if (a<0.0) printf("a has to be positive: gamcdf");
  if (b<0.0) printf("b has to be positive: gamcdf");
  p=_gammp(a,x*b);
  return p>1.0 ? 1.0:p;
}

////////////////////////////////////////// Gamma Auxiliary Functions ////////////////////////////////////////////
#define SMALL 1.0e-250
double _gammln(double xx)
{
    double x,y,tmp,ser;
    static double cof[6]={76.18009172947146,-86.50532032941677,24.01409824083091,-1.231739572450155,
			  0.1208650973866179e-2,-0.5395239384953e-5};
    int j;
    y=x=xx;
    tmp=x+5.5;
    tmp -= (x+0.5)*log(tmp);
    ser=1.000000000190015;
    for (j=0;j<=5;j++) ser += cof[j]/++y;
    return -tmp+log(2.5066282746310005*ser/x);
}

double _gammp(double a,double x)
{
    void _gcf(double *gammcf, double a, double x, double *gln);
    void _gser(double *gamser, double a, double x, double *gln);
    double gamser,gammcf,gln;
    if (x < 0.0 || a <= 0.0) printf("Invalid arguments in routine gammp");
    if (x < (a+1.0)) {
	_gser(&gamser,a,x,&gln);
	return gamser;
    } else {
	_gcf(&gammcf,a,x,&gln);
	return 1.0-gammcf;
    }
}

#define ITMAX 1000
void _gser(double *gamser,double a,double x,double *gln)
{
    double _gammln(double xx);
    int n;
    double sum,del,ap;
    *gln=_gammln(a);
    if (x <= 0.0) {
	if (x < 0.0) printf("x less than 0 in routine gser");
	*gamser=0.0;
	return;
    } else {
	ap=a;
	del=sum=1.0/a;
	for (n=1;n<=ITMAX;n++) {
	    ++ap;
	    del *= x/ap;
	    sum += del;
	    if (fabs(del) < fabs(sum)*EPSILON) {
		*gamser=sum*exp(-x+a*log(x)-(*gln));
		return;
	    }
	}
	printf("a too large, ITMAX too small in routine gser");
	return;
    }
}

#define FPMIN SMALL/EPSILON
void _gcf(double *gammcf, double a, double x, double *gln)
{
    double _gammln(double xx);
    int i;
    double an,b,c,d,del,h;
    *gln=_gammln(a);
    b=x+1.0-a;
    c=1.0/FPMIN;
    d=1.0/b;
    h=d;
    for (i=1;i<=ITMAX;i++) {
	an = -i*(i-a);
	b += 2.0;
	d=an*d+b;
	if (fabs(d) < FPMIN) d=FPMIN;
	c=b+an/c;
	if (fabs(c) < FPMIN) c=FPMIN;
	d=1.0/d;
	del=d*c;
	h *= del;
	if (fabs(del-1.0) < EPSILON) break;
    }
    if (i > ITMAX) printf("a too large, ITMAX too small in gcf");
    *gammcf=exp(-x+a*log(x)-(*gln))*h;
}
#undef FPMIN
#undef ITMAX

#undef SMALL
