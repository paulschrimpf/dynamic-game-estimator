#ifndef _KERNEL_HPP
#define _KERNEL_HPP

#include "linalg.h"
using namespace std;
#include "misc.hpp"


enum kernelType { epan, rbf, gauss2 };

/**  \brief Kernel class -- a fourth order epanechnikov type kernel 
      see gasser, muller, and mammitzsch (1985)
*/
class Kernel {
public:
  int order;
  enum kernelType type;
  Kernel();

  double operator()(const vector<double> &x,const vector<double> &y,
                    double bw,const vector<vector<double> > &R);
  double operator()(const double &x,const double &y, double bw);

  /// returns int_inf^x1 k(x,x2)dx
  double integrate(double x1, double x2, double bw);
  
  /// returns int k(x,x1)*k(x,x2) dx
  double integrate2(double x1,double x2, double bw);
  
  /// returns the derivative with respect to x[d]
  double deriv(const vector<double> &x,const vector<double> &y,
               double bw, const vector<vector<double> > &R,  
               const size_t d);
};

/// Class for finding k nearest neighbors
class neighborInfo {
public:
  double val;
  double dist;
  unsigned int index;
  neighborInfo();
  friend bool operator<(const neighborInfo &a,const neighborInfo &b);
};

/**  \brief Class for approximating policy and value functions

     The class holds the strategy as a function of the state.  A strategy
     is investment as a function of th5Ce state. We approximate strategies using 
     kernel regression.
*/
template<class T>
class ApproxFn {
private:
  T evalnn(const vector<T> &x);
  T eval(const vector<T> &x);
  void evalnnWeights(T *w, const vector<T> &x);
  
public:
  vector<T> val; ///< value of function at data points
  vector<vector<double> > data; ///< points where know value of function
  vector<vector<double> > invRootV; ///< Cov(data)^(-1/2)
  vector<double> mu; ///< estimate of 1/f(x_i)^(dim(x)), used to reweight bandwidth i.e. vkde of (jones)
  double bw;
  int neighbor;
  int calls;
  int nBad;
  int degree;
  Kernel kern; ///< kernel function
  ApproxFn();
  ApproxFn(double bwin);
  
  /// returns the function value in a state 
  T operator()(State<T> &state);
  T operator()(const vector<T> &x); 

  void evalWeights(T *w, const vector<T> &x);
  friend ostream& operator<<(ostream& out, const ApproxFn<T>& s);
  void updateData(const vector<vector<double> > &d);

  T deriv(const vector<T> &x, const size_t d); 
  T deriv(const vector<T> &x, const size_t D, T &value); 

  // computes X'W X and X'W Y at point x, where X = matrix of 
  //   x - data, W = diag(kernel weights) 
  //
  // - if onlyxw is nonzero, computes xw (nx by n) instead of xwy (nx
  //   by 1)
  //
  // - xwx and xwy should be unallocated on entry. On return xwx will
  //   be size nx by nx and xw will be size nx by n  
  void getxwx(double **xwx, double **xwy, unsigned int &nx, 
              unsigned int &N, // output
              const vector<double> &x, const int onlyxw); // input

  // returns derivative*pdf(x) 
  T wDeriv(const vector<T> &x, const size_t d);
  
  void updateMu();
  
  /* Bandwidth selection: Li and Racine (2008) - no automatic way for cdf, so use pdf cv. */
  /**  
       cross validation objective function for bandwidth selection
       see Hall, Li, and Racine (2004) -- min of cvDens is optimal bandwidth 
       for f(val|data) 
       
       warning: modifies this->mu to match bw
  */
  double cvDens(double bw, double bwv);
  
  /* cv for regression */
  double cvReg(double bw);
  
  // for debugging: eval with info printed
  T evalVerbose(const vector<T> &x);
};

/** objective function wrapper for using nlopt for minimization
    in bandwidth selection h*/
double cvObjective(int n, const double *x, double *grad, void *appFn);
double cvObjReg(int n, const double *x, double *grad, void *appFn);

#endif
