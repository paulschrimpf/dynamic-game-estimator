#ifndef _MISC_HPP
#define _MISC_HPP
#include <vector>
#include <iostream>
using std::vector;
using std::ostream;

/** Exponents for a multivariate polynomial ordered as (in 3d)
    0,0,0
    1,0,0
    0,1,0
    0,0,1
    2,0,0
    1,1,0 
    etc
    Used for local polynomial regression
*/
class polyexp {
public:
  vector<int> p;
  polyexp();
  polyexp(int dim); // set to dim dimensions and 0
  void reset(); // reset to zero
  // accessors
  vector<int> operator()();
  int operator[](int j);
  // increment
  vector<int> next();
};

/** 
    Output operator for vectors.
*/
template<class T>
ostream& operator<<(ostream& out, const vector<T>& v);

  
/** \brief State class 
    
    A state is the quality of each firm, x, and epsilon 
*/
template<class T>
class State {
private:
  vector<double> v;
public: 
  T ownQuality;
  vector<double> otherQuality;
  vector<double> x; ///< exogenous state variables
  double eta; ///< private shock
  State();
  /// Constructor
  State(T ownQ, vector<double> q, vector<double> xin, double etain);
  const vector<T> & vec();
  void unvec(vector<T> vec);
};


#endif
