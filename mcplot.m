clear;
close all;
%filenames = {'est_2010-12-16_2014.csv', ...
%             'est_2010-12-17_1247.csv', ...
%             'run/est_2010-12-17_1546.csv'};
%prefix = {'','t20','v2'};
filenames = {'est_2011-12-06_1910.csv',
             'est_2011-12-06_1941.csv'};
% use most recent results
[bar foo] = system('ls run/est*csv -1 | tail -n 1')
foo = foo(1:(end-1));
filenames = { foo };
%filenames = {'run/est_2012-03-29_1616.csv'};
prefix = {'t20v2','t40v2'};
M = [40 ];% 20, 5];
T = [20 ];% 50, 50];
font = 'VarelaRound-Regular';
psi0 = [0.2 0.1 0.2 0.0]; % true values
saveToFile = false;

for f=1:numel(filenames)
  filename = filenames{f};
  dat = csvread(filename,2,0);
  psi = dat(:,2:5);
  bw = dat(:,6);
  bwi = dat(:,7);
  
  %% histograms of estimates
  for k=1:3
    figure;
    colormap('copper');
    hist(psi(:,k))
    h = findobj(gca,'Type','patch');
    set(h,'FaceColor','w','EdgeColor','k')
    xlabel(sprintf('\\psi_%d',k-1));
    ylabel('count');
    yl = get(gca,'YLim');
    set(gca,'YLim',[yl(1), yl(2)+1]);
    set(gca,'FontName',font);
    line([psi0(k), psi0(k)],get(gca,'YLim'),'LineWidth',3,'Color','r');
    xl = get(gca,'XLim');
    if abs(xl(1)-psi0(k))<0.1*(xl(2)-xl(1));
      xl(1) = psi0(k) - 0.1*(xl(2)-xl(1));
      xlim(xl);
    elseif abs(xl(2)-psi0(k))<0.1*(xl(2)-xl(1));
      xl(2) = psi0(k) + 0.1*(xl(2)-xl(1));
      xlim(xl);
    end
    figname = sprintf('figures/%spsi%dhist',prefix{f},k-1);
    if (saveToFile)
      print('-depsc2',figname);
      system(sprintf('epstopdf %s.eps -o=%s.pdf',figname,figname));
    end
  end % for k
  
  if (saveToFile)
    out = fopen(sprintf('tables/%sest.tex',prefix{f}),'w');
    fprintf(out,'True Value'); fprintf(out,' & %g',psi0); fprintf(out,'\\\\ \n');
    fprintf(out,'Mean'); fprintf(out,' & %.2g',mean(psi)); 
    fprintf(out,'\\\\ \n');
    fprintf(out,'S.D.'); fprintf(out,' & %.2g',std(psi)); 
    fprintf(out,'\\\\ \n');
    fprintf(out,'Bias'); fprintf(out,' & %.2g',mean(psi-ones(size(psi,1),1)*psi0)); 
    fprintf(out,'\\\\ \n');
    fprintf(out,'M.S.E.'); fprintf(out,' & %.2g',mean((psi-ones(size(psi,1),1)*psi0).^2)); 
    fprintf(out,'\\\\ \n');
    fclose(out);
  end
end % for (f)